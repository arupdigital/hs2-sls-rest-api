const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const apiRouter = require('./routes/api');
const { notFound, badRequest } = require('boom');
const { handle400, handle401, handle403, handle404, handle409, handle500 } = require('./errors');
const { verifyJWT } = require('./auth');

app.use(cors());
app.use(bodyParser.json());
app.use(express.static('public'));

if (process.env.NODE_ENV !== 'test') app.use(verifyJWT);

app.use('/', apiRouter);

app.use('/*', (req, res, next) => next(notFound('Path not found')));

app.use((err, req, res, next) => {
  if (err instanceof SyntaxError) next(badRequest('Invalid JSON'));
  else next(err);
});

app.use(handle400);
app.use(handle401);
app.use(handle403);
app.use(handle404);
app.use(handle409);
app.use(handle500);

module.exports = app;
const {Interest} = require('../../models/geo');
const {Interest: arangoInterest} = require('../../models/collection');
const {notFound, badRequest} = require('boom');

function getInterestGeoById (req, res, next) {
  const {objectId} = req.params;
  Interest.findById(objectId)
    .then(int => {
      if(int) res.status(200).send(int);
      else throw notFound('Interest not found');
    })
    .catch(next);
}

function getAllInterestGeo (req, res, next) {
  const page = Number(req.query.page);
  if(page) {
    if(page < 1 || !Number.isInteger(page)) throw notFound('Page limit exceeded');
    const limit = 50;
    Interest.findAndCountAll({
      limit,
      offset: limit * (page - 1)
    })
      .then(result => {
        const interests = result.rows;
        const totalPages = Math.ceil(result.count / limit);
        if(page > totalPages) throw notFound('Page limit exceeded');
        res.status(200).send({interests, page, totalPages});
      })
      .catch(next);
  } else {
    Interest.findAll()
      .then(interests => res.status(200).send({interests}))
      .catch(next);
  }
}

function postInterestGeo (req, res, next) {
  const {i_object_id} = req.body;
  if(typeof i_object_id !== 'number') next(badRequest('Valid "i_object_id" required'));
  else arangoInterest.firstExample({i_object_id})
    .then(doc => {
      if(!doc) throw notFound('Interest must be created before adding geo data');
      return Interest.create(req.body);
    })
    .then(result => res.status(200).send(result))
    .catch(next);
}

function updateInterestGeo (req, res, next) {
  const {i_object_id, i_geometry} = req.body;
  if(!i_geometry || !Number.isInteger(i_object_id)) next(badRequest('Body must contain valid i_object_id and i_geometry key-value pairs'));
  else {
    Interest.findById(i_object_id)
      .then(result => {
        if(!result) throw notFound('Interest not found');
        return Interest.upsert(req.body);
      })
      .then(() => res.status(200).send({message: `${i_object_id} has been updated`}))
      .catch(next);
  }
}

function deleteInterestGeo (req, res, next) {
  const {objectId} = req.params;
  Interest.findById(objectId)
    .then(result => {
      if(!result) throw notFound('Interest not found');
      return Interest.destroy({where:{i_object_id: objectId}});
    })
    .then(() => res.status(200).send({message: `${objectId} has been deleted`}))
    .catch(next);
}

module.exports = {
  getInterestGeoById,
  getAllInterestGeo,
  postInterestGeo,
  updateInterestGeo,
  deleteInterestGeo
};

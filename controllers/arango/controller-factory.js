const dbArango = require('../../db/db').getArangoDB();
const dbPG = require('../../models/geo');
const models = require('../../models/collection');
const graph = require('../../db/db').getArangoGraph();
const { notFound, badRequest } = require('boom');
const { getAllQuery, getByKeyQuery } = require('./aql');
const { edgeConfig, edgeRenameRef } = require('./edgeConfig');

const {
  controllerFunc,
  deleteController,
  pagination,
  processResponse,
  getFilters,
  getSortStr
} = require('../../utils/controller');

const validationOff = false;

const controllerFactory = (vertColName, edges) => {
  return {
    getAll: {
      controller: (req, res, next) => {
        if (req.query && !Object.values(req.query).every(ele => typeof ele === 'string')) throw badRequest('Invalid query');
        const fields = req.query.fields ? req.query.fields.split(',') : [];
        const incDel = req.query.deleted === 'true';
        const { sort_by: sortBy } = req.query;
        const depth = req.query.deep === 'true' ? '2' : '1';
        const { page, skip, limit } = pagination(req.query.page, req.query.limit);
        const sortStr = sortBy ? getSortStr(sortBy) : null;
        const filters = getFilters(req.query, edgeRenameRef, vertColName);
        const queryStr = getAllQuery(vertColName, incDel, depth, filters, sortStr);
        const arangoPromise = dbArango.query(queryStr, { page, skip, limit })
          .then(({ _result: [result] }) => {
            const { page, totalPages } = result;
            if (!result) throw notFound('Document not found');
            if (page > totalPages) throw notFound(`Selected page (${page}) exceeds total pages (${totalPages})`);
            else {
              result.data = processResponse(result.data, edgeRenameRef, fields, vertColName);
              if (result.data.length !== result.count) result.filteredCount = result.data.length;
              return result;
            }
          });

        if (dbPG[vertColName] !== undefined) {
          const pgPromise = dbPG[vertColName].findAll();

          Promise.all([arangoPromise, pgPromise])
            .then(([arangoObj, pgObj]) => {
              const prefix = vertColName.charAt(0).toLowerCase();
              const objectId = `${prefix}_object_id`;
              const geoKey = `${prefix}_geometry`;
              arangoObj.data.forEach(arangoColl => {
                pgObj.forEach(pgColl => {
                  if (pgColl.dataValues[objectId] === arangoColl[objectId] && pgColl.dataValues[geoKey]) {
                    arangoColl.geoData = pgColl.dataValues[geoKey];
                  }
                });
              });
              res.status(200).send(arangoObj);
            })
            .catch(next);
        }
        else {
          Promise.resolve(arangoPromise)
            .then(arangoObj => {
              res.status(200).send(arangoObj);
            })
            .catch(next);
        }
      },
      method: 'get',
      path: '/'
    },

    getByKey: {
      controller: (req, res, next) => {
        const id = `${vertColName}/${req.params.key}`;
        const fields = req.query.fields ? req.query.fields.split(',') : [];
        const depth = req.query.deep === 'true' ? '2' : '1';
        const incDel = req.query.deleted === 'true';
        const arangoPromise = dbArango.query(getByKeyQuery(id, incDel, depth))
          .then(({ _result: [result] }) => {
            if (!result) throw notFound('Document not found');
            else if (result._del && !incDel) throw notFound('Document has been deleted');

            return processResponse([result], edgeRenameRef, fields, vertColName)[0];
          });

        Promise.resolve(arangoPromise)
          .then(arangoObj => {
            const prefix = vertColName.charAt(0).toLowerCase();
            const objectIdKey = `${prefix}_object_id`;
            const geoKey = `${prefix}_geometry`;
            const id = arangoObj[objectIdKey];
            if (dbPG[vertColName]) {
              dbPG[vertColName].findOne({ where: { [`${prefix}_object_id`]: id } })
                .then(result => {
                  if (result) arangoObj.geoData = result.dataValues[geoKey];
                  res.status(200).send(arangoObj);
                });
            }
            else res.status(200).send(arangoObj);
          })
          .catch(next);
      },

      method: 'get',
      path: '/:key'
    },

    post: {
      controller: (req, res, next) => {
        const { body } = req;
        controllerFunc(graph, vertColName, body, edges, edgeRenameRef, 'post', undefined, validationOff)
          .then(doc => {
            res.status(201).send(doc);
          })
          .catch(next);
      },
      method: 'post',
      path: '/'
    },

    putByKey: {
      controller: (req, res, next) => {
        const { body } = req;
        const id = `${vertColName}/${req.params.key}`;

        controllerFunc(graph, vertColName, body, edges, edgeRenameRef, 'put', id, validationOff)
          .then(doc => res.status(201).send(doc))
          .catch(next);
      },
      method: 'put',
      path: '/:key'
    },

    patchByKey: {
      controller: (req, res, next) => {
        const { body } = req;
        const id = `${vertColName}/${req.params.key}`;

        controllerFunc(graph, vertColName, body, edges, edgeRenameRef, 'patch', id, validationOff)
          .then(doc => res.status(201).send(doc))
          .catch(next);
      },
      method: 'patch',
      path: '/:key'
    },

    deleteByKey: {
      controller: (req, res, next) => {
        const id = `${vertColName}/${req.params.key}`;
        const { restore } = req.query;

        deleteController(vertColName, id, restore)
          .then(doc => res.status(200).send(doc))
          .catch(next);
      },
      method: 'delete',
      path: '/:key'
    }
  };
};

const makeControllers = () => {
  return Object.keys(models).reduce((acc, name) => {
    const edges = edgeConfig[name] || [];
    acc[name] = controllerFactory(name, edges);
    return acc;
  }, {});
};

module.exports = makeControllers();
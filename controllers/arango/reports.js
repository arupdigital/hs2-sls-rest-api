const { getArangoDB } = require('../../db/db');
const { licenceTrackerQuery, weeklyReportQuery } = require('./aql');
const { notFound } = require('boom');
const db = getArangoDB();
const { licenceTrackerColumns, weeklyReportColumns } = require('../../refs/reports');
const { createExportRow } = require('../../utils');


const fetchLicenceTracker = async (req, res, next) => {
  try {
    const { _result: data } = await db.query(licenceTrackerQuery);
    if (!data) throw notFound('Document not found.');
    const tableRows = data.map(item => createExportRow(item, licenceTrackerColumns));
    res.send(tableRows);
  }
  catch (err) {
    next(err);
  }
};



const fetchWeeklyReport = async (req, res, next) => {
  try {
    const { _result: data } = await db.query(weeklyReportQuery);
    if (!data) throw notFound('Document not found.');
    const tableRows = data.map(item => createExportRow(item, weeklyReportColumns));
    res.send(tableRows);
  }
  catch (err) {
    next(err);
  }
};


module.exports = {
  fetchLicenceTracker,
  fetchWeeklyReport,
};

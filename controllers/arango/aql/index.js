module.exports = {
  getAllQuery: require('./getAll'),
  getByKeyQuery: require('./getByKey'),
  licenceTrackerQuery: require('./licenceTracker'),
  weeklyReportQuery: require('./weeklyReportQuery'),
};
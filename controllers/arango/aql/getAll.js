module.exports = (colName, incDel, depth, filters, sort) => {
  return (
    'LET data = ( ' +
      'FOR node in ' + colName + ' ' +
        (!incDel || filters.root ? 'FILTER ' : '') + (!incDel ? '!node._del ' : '') + (!incDel && filters.root ? '&& ' : '') + (filters.root || '') +
        'LET nodes = ( ' +
          'FOR v, e IN 1..' + depth + ' OUTBOUND node._id GRAPH \'all\' ' +
            'FILTER v != null && e != null' + (!incDel ? ' && !v._del ' : ' ') +
            'RETURN MERGE(v, UNSET(e, \'_to\', \'_id\', \'_rev\', \'_key\'), {edgeCollection: PARSE_IDENTIFIER(e).collection}) ' +
        ') ' +
        'LET merged = MERGE(node, {linked:nodes}) ' +
        (filters.linked.length > 0 ?
          'LET filteredLinked = ( ' +
            'FOR linkedDoc IN merged.linked ' +
              filters.linked +
              'RETURN linkedDoc ' +
          ') ' +
          'FILTER LENGTH(filteredLinked) == LENGTH(merged.linked) '
          : '') +
        'RETURN merged ' +
    ') ' +
    'LET paginated = (FOR doc IN data ' + (sort ? 'SORT ' + sort : '') + ' LIMIT @skip, @limit RETURN doc) ' +
    'LET totalCount = LENGTH(data) ' +
    'LET totalPages = CEIL(totalCount / @limit) ' + 
    'RETURN {data:paginated, page:(@page || 1), totalPages: (totalPages || 1), count:LENGTH(data), totalCount}'
  );
};
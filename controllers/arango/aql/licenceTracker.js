module.exports = `
FOR l IN Licence
        
      LET licence = (
          RETURN {
              l_acc_number: l.l_acc_number,
              l_cro_number: l.l_cro_number,
              l_pln_number: l.l_pln_number,
              l_expiry_date: l.l_expiry_date,
              l_date_posted: l.l_date_posted,
              l_date_signed_by_lo: l.l_date_signed_by_lo,
              l_approved_countersign_date: l.l_approved_countersign_date,
              l_date_countersigned: l.l_date_countersigned,
              l_payment_schedule_approval_date: l.l_payment_schedule_approval_date,
              l_arup_general_comment: l.l_arup_general_comment,
              l_issue_status: l.l_issue_status,
              l_site_visits: l.l_site_visits
          }
      )
      
      LET status = (
          FOR status IN 1..1 OUTBOUND l has_status
              RETURN status.ls_status
      )
      
      LET escalation_tier = (
          FOR tier IN 1..1 OUTBOUND l has_escalation_tier
              RETURN tier.et_tier
      )
      
      LET licence_group = (
          FOR group IN 1..1 OUTBOUND l has_group
              RETURN group.lg_name
      )
      
      LET surveyor = (
          FOR s IN 1..1 OUTBOUND l collected_by
              Return s.sv_name
      )
      
      LET interests = (
          FOR i IN 1..1 OUTBOUND l has_interest
            FOR agent IN 1..1 OUTBOUND i has_agent
              LET address_array_interest = REMOVE_VALUES([ i.i_building_number, i.i_building_name, i.i_address_line_1, i.i_address_line_2, i.i_address_line_3, i.i_city, i.i_postcode, i.i_county, i.i_country], null)
              LET address_array_agent = REMOVE_VALUES([agent.p_apartment_number, agent.p_house_number, agent.p_house_name, agent.p_address_line_1, agent.p_address_line_2, agent.p_address_line_3, agent.p_city, agent.p_postcode, agent.p_county, agent.p_country], null)
              LET full_name = REMOVE_VALUES([agent.p_title, agent.p_first_name, agent.p_middle_names, agent.p_last_name], null)
              LET contact_array = (['Home:', agent.p_home_tel, 'Work:', agent.p_work_tel, 'Fax:', agent.p_fax, 'Mobile:', agent.p_mobile])
             
              return {
                  agent_name: CONCAT_SEPARATOR(' ', full_name),
                  agent_address: CONCAT_SEPARATOR(', ', address_array_agent),
                  agent_contact_details: CONCAT_SEPARATOR(' ', contact_array),
                  agent_email: agent.p_email,
                  _id: i._id,
                  i_title_id: i.i_title_id,
                  address_field: CONCAT_SEPARATOR(', ', address_array_interest)
              }
      )
        
      LET sites = (
          FOR i IN interests
              FOR s IN 1..1 INBOUND i contains_interest
                FOR contractArea IN 1..1 OUTBOUND s has_contract_area
                    LET crop_type_height = (['Height:', s.s_crop_height, 'Type:', s.s_crop_type ])
                  RETURN {
                        contract_area: contractArea.c_area,
                        s_access_requirements: s.s_access_requirments,
                        s_hse_factors: s.s_hse_factors,
                        s_land_use: s.s_land_use,
                        s_crop_type_height: CONCAT_SEPARATOR(' ', crop_type_height),
                        s_water_course_on_site: s.s_water_course_on_site,
                        s_watercourse_details: s.s_watercourse_details,
                        s_within_public_highway_details: s.s_within_public_highway_details,
                        s_other_activities: s.s_other_activities,
                        s_phone_signal_status: s.s_phone_signal_status,
                        s_access_date_time: s.s_access_date_time,
                        s_asbestos_details: s.s_asbestos_details,
                        s_confined_restricted_details: s.s_confined_restricted_details,
                        s_railway_details: s.s_railway_details,
                        s_unbranded_high_vis_details: s.s_unbranded_high_vis_details,
                        s_is_measham: s.s_is_measham,
                        s_priority_level: s.s_priority_level,
                        s_is_cdes: s.s_is_cdes,
                        s_id: s.s_id
                  }
      )
      
      LET contract_area = (
        FOR area IN sites
            RETURN area.contract_area
      )
      
      LET ees_ids = (
        FOR ees In sites
            FILTER ees.s_is_cdes == false
            RETURN ees.s_id
      )
      
      LET no_of_ees_ids = (
        RETURN LENGTH(ees_ids)
      )
      
       LET cdes_ids = (
        FOR cdes In sites
            FILTER cdes.s_is_cdes == true
            RETURN cdes.s_id
      )
      
      LET no_of_cdes_ids = (
        RETURN LENGTH(cdes_ids)
      )
        
      LET contact_logs = (
        FOR log IN 1..1 INBOUND l relates_to
        LET contact_log_array = (['Date:', LENGTH(log.cl_date) > 0 ? CONCAT_SEPARATOR('-', REVERSE(SPLIT(log.cl_date, '-'))) : null, 'Summary:', log.cl_summary ])
        RETURN SUBSTITUTE(CONCAT_SEPARATOR(' ', contact_log_array), ['\n', '\r'], '')
      )
      
      LET amount_of_contact_events = (
        RETURN LENGTH(contact_logs)
      )
      
      LET party_concerns = (
        FOR concern IN 1..1 INBOUND l related_licence
            FOR reported, reported_date IN 1..1 OUTBOUND concern reported_by
            FOR actioned, actioned_date IN 1..1 OUTBOUND concern actioned_by
            RETURN {
            pc_concern: concern.pc_concern,
            reported_by: CONCAT_SEPARATOR(' ', [reported.p_title, reported.p_first_name, reported.p_middle_names, reported.p_last_name]),
            reported_date: LENGTH(reported_date.date_reported) > 0 ? CONCAT_SEPARATOR('-', REVERSE(SPLIT(SPLIT(reported_date.date_reported, 'T')[0], '-'))) : null,
            actioned_by: CONCAT_SEPARATOR(' ', [actioned.p_title, actioned.p_first_name, actioned.p_middle_names, actioned.p_last_name]),
            actioned_date: LENGTH(actioned_date.date_actioned) > 0 ? CONCAT_SEPARATOR('-', REVERSE(SPLIT(SPLIT(actioned_date.date_actioned, 'T')[0], '-'))) : null
            }
      )
      
      
      LET licencee = (
        FOR interest IN interests
            FOR p, e IN 1..1 INBOUND interest has_interest
                LET peopleDetails = (
                   
                    FOR person IN 1..1 OUTBOUND p has_member
                        FILTER person.p_first_name != null
                         LET address_array = REMOVE_VALUES([person.p_apartment_number, person.p_house_number, person.p_house_name, person.p_address_line_1, person.p_address_line_2, person.p_address_line_3, person.p_city, person.p_county, person.p_country], null)
                         LET full_name = REMOVE_VALUES([person.p_first_name, person.p_middle_names, person.p_last_name], null)
                        FOR agent IN 1..1 OUTBOUND person has_agent_org
                        RETURN {
                            ao_name: agent.ao_name,
                            pa_is_current: p.pa_is_current,
                            pa_is_mlo: p.pa_is_mlo,
                            pa_name: p.pa_name,
                            tenure: e.tenure,
                            is_occupier: e.is_occupier,
                            full_name: CONCAT_SEPARATOR(' ', full_name),
                            p_first_name: person.p_first_name,
                            p_middle_names: person.p_middle_names,
                            p_last_name: person.p_last_name,
                            address_field: CONCAT_SEPARATOR(', ', address_array)
                        }
                    )
                    LET orgDetails = (
                        FOR org IN 1..1 OUTBOUND p has_member
                            FILTER org.o_name != null
                            LET address_array_org = REMOVE_VALUES([org.p_apartment_number, org.p_house_name, org.p_address_line_1, org.p_address_line_2, org.p_address_line_3, org.p_city, org.p_county, org.p_country], null)
                            FOR contact, edge IN 1..1 OUTBOUND org has_contact
                                FILTER edge.is_primary == true
                             LET address_array_contact = REMOVE_VALUES([contact.p_apartment_number, contact.p_house_number, contact.p_house_name, contact.p_address_line_1, contact.p_address_line_2, contact.p_address_line_3, contact.p_city, contact.p_county, contact.p_country], null)
                            LET full_name = REMOVE_VALUES([contact.p_title, contact.p_first_name, contact.p_middle_names, contact.p_last_name], null)
                            LET contact_array = (['Home:', contact.p_home_tel, 'Work:', contact.p_work_tel, 'Mobile:', contact.p_mobile])
                            RETURN {
                                contact: {
                                    p_title: contact.p_title,
                                    p_first_name: contact.p_first_name,
                                    p_email: contact.p_email,
                                    is_primary: edge.is_primary,
                                    full_name: CONCAT_SEPARATOR(' ', full_name),
                                    address_field: CONCAT_SEPARATOR(', ', address_array_contact),
                                    contact_details: CONCAT_SEPARATOR(' ', contact_array)
                                },
                                pa_is_current: p.pa_is_current,
                                pa_is_mlo: p.pa_is_mlo,
                                pa_name: p.pa_name,
                                tenure: e.tenure,
                                is_occupier: e.is_occupier,
                                o_name: org.p_first_name,
                                address_field: CONCAT_SEPARATOR(', ', address_array_org)
                            }
                    )
                    RETURN APPEND(peopleDetails, orgDetails)
      )
            





    
      
        
    RETURN {
        l_acc_number: l.l_acc_number,
        l_cro_number: l.l_cro_number,
        l_pln_number: l.l_pln_number,
        l_expiry_date: LENGTH(l.l_expiry_date) > 0 ? CONCAT_SEPARATOR('-', REVERSE(SPLIT(l.l_expiry_date, '-'))) : l.l_expiry_date,
        l_date_posted: LENGTH(l.l_date_posted) > 0 ? CONCAT_SEPARATOR('-', REVERSE(SPLIT(l.l_date_posted, '-'))) : l.l_date_posted,
        l_date_signed_by_lo: LENGTH(l.l_date_signed_by_lo) > 0 ? CONCAT_SEPARATOR('-', REVERSE(SPLIT(l.l_date_signed_by_lo, '-'))) : l.l_date_signed_by_lo,
        l_approved_countersign_date: LENGTH(l.l_approved_countersign_date) > 0 ? CONCAT_SEPARATOR('-', REVERSE(SPLIT(l.l_approved_countersign_date, '-'))) : l.l_approved_countersign_date,
        l_date_countersigned: LENGTH(l.l_date_countersigned) > 0 ? CONCAT_SEPARATOR('-', REVERSE(SPLIT(l.l_date_countersigned, '-'))) : l.l_date_countersigned,
        l_payment_schedule_approval_date:LENGTH(l.l_payment_schedule_approval_date) > 0 ? CONCAT_SEPARATOR('-', REVERSE(SPLIT(l.l_payment_schedule_approval_date, '-'))) : l.l_payment_schedule_approval_date,
        l_arup_general_comment: SUBSTITUTE(l.l_arup_general_comment, ['\n', '\r'], ''),
        l_issue_status: SUBSTITUTE(l.l_issue_status, ['\n', '\r'], ''),
        l_site_visits: l.l_site_visits,
        status: status[0],
        escalation_tier: escalation_tier[0],
        licence_group: licence_group[0],
        surveyor: surveyor[0],
        //interests: IS_ARRAY(interests) && LENGTH(interests) == 0 ? null : interests, 
        //sites: IS_ARRAY(sites) && LENGTH(sites) == 0 ? null : sites,
        //contact_logs: IS_ARRAY(contact_logs) && LENGTH(contact_logs) == 0 ? null : contact_logs, 
        licencee: IS_ARRAY(licencee[**]) && LENGTH(licencee[**]) == 0 ? null : licencee[**],
        ees_ids: IS_ARRAY(ees_ids) && LENGTH(ees_ids) == 0 ? null : ees_ids,
        cdes_ids: IS_ARRAY(cdes_ids) && LENGTH(cdes_ids) == 0 ? null : cdes_ids,
        no_of_ees_ids: no_of_ees_ids[0],
        no_of_cdes_ids: no_of_cdes_ids[0],
        amount_of_contact_events: amount_of_contact_events[0],
        contract_area: IS_ARRAY(contract_area) && LENGTH(contract_area) == 0 ? null : contract_area,
        //party_concerns: IS_ARRAY(party_concerns) && LENGTH(party_concerns) == 0 ? null : party_concerns,
        site_address: interests[* RETURN CONCAT(CURRENT.i_title_id, ': ', CURRENT.address_field)],
        title_id: interests[*].i_title_id,
        agent_name: interests[* RETURN CONCAT( CURRENT.i_title_id, ': ', CURRENT.agent_name)],
        agent_address: interests[* RETURN CONCAT(CURRENT.i_title_id, ': ', CURRENT.agent_address)],
        agent_contact_details: interests[* RETURN CONCAT(CURRENT.i_title_id, ': ', CURRENT.agent_contact_details)],
        agent_email: interests[* RETURN CONCAT(CURRENT.i_title_id, ': ', CURRENT.agent_email)],
        contact_logs: IS_ARRAY(contact_logs) && LENGTH(contact_logs) > 0 ? CONCAT_SEPARATOR(' ', contact_logs) : null,
        pc_concern: party_concerns[*].pc_concern,
        reported_by: party_concerns[*].reported_by,
        reported_date: party_concerns[*].reported_date,
        actioned_by: party_concerns[*].actioned_by,
        actioned_date: party_concerns[*].actioned_date,
        licencee_address: licencee[** RETURN IS_OBJECT(CURRENT.contact) == true ? CURRENT.contact.address_field : CURRENT.address_field],
        licencee_name: licencee[** RETURN LENGTH(CURRENT.o_name) > 0 ? CURRENT.o_name : IS_OBJECT(CURRENT.contact) == true ? CURRENT.contact.full_name : CURRENT.full_name],
        licencee_contact_details: licencee[** RETURN IS_OBJECT(CURRENT.contact) == true ? CURRENT.contact.contact_details : 'null'],
        licencee_email: licencee[** RETURN IS_OBJECT(CURRENT.contact) == true ? CURRENT.contact.p_email : 'null'],
        contract_area: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.contract_area) > 0 ? CURRENT.contract_area : 'null')],
        s_access_requirements: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_access_requirements) > 0 ? CURRENT.s_access_requirements : 'null')],
        s_hse_factors: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_hse_factors) > 0 ? CURRENT.s_hse_factors : 'null')],
        s_land_use: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_land_use) > 0 ? CURRENT.s_land_use : 'null')],
        s_crop_type_height: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_crop_type_height) > 0 ? CURRENT.s_crop_type_height : 'null')],
        s_water_course_on_site: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_water_course_on_site) > 0 ? CURRENT.s_water_course_on_site : 'null')],
        s_water_course_details: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_water_course_details) > 0 ? CURRENT.s_water_course_details : 'null')],
        s_within_public_highway_details: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_within_public_highway_details) > 0 ? CURRENT.s_within_public_highway_details : 'null')],
        s_other_activities: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_other_activities) > 0 ? CURRENT.s_other_activities : 'null')],
        s_phone_signal_status: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_phone_signal_status) > 0 ? CURRENT.s_phone_signal_status : 'null')],
        s_access_date_time: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_access_date_time) > 0 ? CURRENT.s_access_date_time : 'null')],
        s_asbestos_details: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_asbestos_details) > 0 ? CURRENT.s_asbestos_details : 'null')],
        s_confined_restricted_details: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_confined_restricted_details) > 0 ? CURRENT.s_confined_restricted_details : 'null')],
        s_railway_details: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_railway_details) > 0 ? CURRENT.s_railway_details : 'null')],
        s_unbranded_high_vis_details: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_unbranded_high_vis_details) > 0 ? CURRENT.s_unbranded_high_vis_details : 'null')],
        s_is_measham: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_is_measham) > 0 ? CURRENT.s_is_measham : 'null')],
        s_priority_level: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_priority_level) > 0 ? CURRENT.s_priority_level : 'null')],
        s_is_cdes: sites[* RETURN CONCAT(CURRENT.s_id, ': ', LENGTH(CURRENT.s_is_cdes) > 0 ? CURRENT.s_is_cdes : 'null')],
        pa_is_mlo: licencee[** LIMIT 1].pa_is_mlo == true ? 'true' : 'false',
        pa_name: LENGTH(licencee[** LIMIT 1].pa_name) > 0 ? licencee[** LIMIT 1].pa_name : 'null',
        tenure: LENGTH(licencee[** LIMIT 1].tenure) > 0 ? licencee[** LIMIT 1].tenure : 'null',
        ao_name: LENGTH(licencee[** LIMIT 1].pa_name) > 0 ? licencee[** LIMIT 1].ao_name : 'null'
    
        
    }


`;


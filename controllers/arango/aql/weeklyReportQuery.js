module.exports = `
FOR edge in has_interest
    LET interest = (
        FOR interest IN Interest
            FILTER interest._id == edge._to
            RETURN interest
    )[0]
    
    
    LET licence = (
        FOR licence IN Licence
            FILTER licence._id == edge._from
            RETURN licence
    )[0]
    
    
    LET licenceGroup = (
        FOR group IN 1..1 OUTBOUND licence has_group
            RETURN group.lg_name
    )[0]
    
    
    LET licenceStatus = (
        FOR status IN 1..1 OUTBOUND licence has_status
            RETURN status.ls_status
    )[0]
    
    
    LET escalationTier = (
        FOR tier IN 1..1 OUTBOUND licence has_escalation_tier
            RETURN tier.et_tier
    )[0]
    
    
    // NOT WORKING - GETTING EMPTY ARRAY EVERY TIME
    LET siteInfo = (
        FOR site IN 1..1 INBOUND interest contains_interest
            LET surveyTypes = (
                FOR st in 1..1 OUTBOUND site has_survey_type
                    RETURN DISTINCT st.st_id
            )
            
            LET contractAreas = (
                FOR ca IN 1..1 OUTBOUND site has_contract_area
                    RETURN DISTINCT ca.c_area
            )
            
            RETURN { 
                surveyTypes,
                contractAreas,
                s_priority_level: site.s_priority_level,
                s_phone_signal_status: site.s_phone_signal_status
            }
    )
    
    
    LET relatedInterests = (
        FOR i IN 1..1 OUTBOUND licence has_interest
            RETURN i.i_title_id
    )
    
    
    LET partyMembers = (
        FOR party IN 1..1 OUTBOUND licence has_licencee
            
            LET people = (
                FOR person IN 1..1 OUTBOUND party has_member
                    FILTER person.p_first_name != null
                    
                    LET address = [
                        person.p_apartment_number,
                        person.p_house_name,
                        person.p_house_number,
                        person.p_address_line_1,
                        person.p_address_line_2,
                        person.p_address_line_3,
                        person.p_county,
                        person.p_postcode,
                        person.p_country
                    ]
                    
                    LET names = [person.p_title, person.p_first_name, person.p_middle_names, person.p_last_name]
                    
                    RETURN {
                        full_name: CONCAT_SEPARATOR(' ', names),
                        p_title: person.p_title,
                        p_first_name: person.p_first_name,
                        p_middle_names: person.p_middle_names,
                        p_last_name: person.p_last_name,
                        p_home_tel: person.p_home_tel,
                        p_work_tel: person.p_work_tel,
                        p_mobile: person.p_mobile,
                        address: CONCAT_SEPARATOR(', ', address),
                        contact_number: person.p_mobile != null ? person.p_mobile : person.p_home_tel != null ? person.p_home_tel : person.p_work_tel
                    }
            )
        
            LET orgs = (
                FOR org in 1..1 OUTBOUND party has_member
                    FILTER org.o_name != null
                    
                    LET primaryContacts = (
                        FOR contact, e IN 1..1 OUTBOUND org has_contact
                            FILTER e.is_primary == true

                            LET address = [
                                contact.p_apartment_number,
                                contact.p_house_name,
                                contact.p_house_number,
                                contact.p_address_line_1,
                                contact.p_address_line_2,
                                contact.p_address_line_3,
                                contact.p_county,
                                contact.p_postcode,
                                contact.p_country
                            ]
                            
                            LET names = [contact.p_title, contact.p_first_name, contact.p_middle_names, contact.p_last_name]
                    
                            RETURN {
                                full_name: CONCAT_SEPARATOR(' ', names),
                                p_title: contact.p_title,
                                p_first_name: contact.p_first_name,
                                p_middle_names: contact.p_middle_names,
                                p_last_name: contact.p_last_name,
                                p_home_tel: contact.p_home_tel,
                                p_work_tel: contact.p_work_tel,
                                p_mobile: contact.p_mobile,
                                address: CONCAT_SEPARATOR(', ', address),
                                contact_number: contact.p_mobile != null ? contact.p_mobile : contact.p_home_tel != null ? contact.p_home_tel : contact.p_work_tel
                            }
                    )
                    
                    LET address = REMOVE_VALUES([
                        org.o_building_name,
                        org.o_building_number,
                        org.o_address_line_1,
                        org.o_address_line_2,
                        org.o_address_line_3,
                        org.o_county,
                        org.o_country
                    ], null)
                    
                    
                    RETURN {
                        o_name: org.o_name,
                        o_tel_number: org.o_tel_number,
                        address: CONCAT_SEPARATOR(', ', address),
                        primary_contacts: primaryContacts
                    }
            )
        
            RETURN APPEND(orgs, people)
    )[0]


    LET licencee = (
        FOR party IN 1..1 OUTBOUND licence has_licencee
            RETURN party
    )[0]
    
    
    LET tenure = (
        FOR e in has_interest
            FILTER e._from == licencee._id AND e._to == edge._to
            RETURN e.tenure
    )[0]
    
    
    LET primaryContacts = (
        FOR member in partyMembers
            FILTER LENGTH(member.primaryContacts) > 0
            RETURN member
    )
    
    
    LET agent = (
        FOR agent IN 1..1 OUTBOUND interest has_agent
            LET address = [
                agent.p_apartment_number,
                agent.p_house_name,
                agent.p_house_number,
                agent.p_address_line_1,
                agent.p_address_line_2,
                agent.p_address_line_3,
                agent.p_county,
                agent.p_postcode,
                agent.p_country
            ]
            
            LET names = [agent.p_title, agent.p_first_name, agent.p_middle_names, agent.p_last_name]
    
            RETURN {
                full_name: CONCAT_SEPARATOR(' ', names),
                p_title: agent.p_title,
                p_first_name: agent.p_first_name,
                p_middle_names: agent.p_middle_names,
                p_last_name: agent.p_last_name,
                p_home_tel: agent.p_home_tel,
                p_work_tel: agent.p_work_tel,
                p_mobile: agent.p_mobile,
                address: CONCAT_SEPARATOR(', ', address)
            }
    )[0]
    
    
    LET siteAddress = CONCAT_SEPARATOR(', ', [
        interest.i_building_name,
        interest.i_building_number,
        interest.i_address_line_1,
        interest.i_address_line_2,
        interest.i_address_line_3,
        interest.i_county,
        interest.i_country
    ])
    
    
    LET siteUnlocked = (
        DATE_MILLISECOND(licence.l_expiry_date) < DATE_MILLISECOND(DATE_NOW()) 
        AND licenceStatus == 'Agreed'
    ) 
    ? 'Yes'
    : 'No'
    
    
    // HIGHEST CDES PRIORITY
    LET cdesPriority = POSITION(siteInfo[*].s_priority_level, 'High') == true 
        ? 'High' 
        : POSITION(siteInfo[*].s_priority_level, 'Medium') == true 
            ? 'Medium' 
            : 'Low'
    
    
    // LOWEST SIGNAL STATUS
    LET phoneSignal = POSITION(siteInfo[*].s_phone_signal_status, 'Low') == true 
        ? 'Low' 
        : POSITION(siteInfo[*].s_phone_signal_status, 'Medium') == true 
            ? 'Medium' 
            : 'High'
    
    
    
    RETURN {
        title_id: interest.i_title_id,
        //related_titles: CONCAT_SEPARATOR(', ', relatedInterests),
        party_id: licencee.pa_ref,
        licence_acc_number: licence.l_acc_number,
        licence_group: licenceGroup,
        contract_areas: CONCAT_SEPARATOR(', ', UNIQUE(siteInfo[*].contractAreas[**])),
        site_address: siteAddress,
        // area_m2: 
        // land_type:
        land_owner_name: licencee.pa_name,
        agent_name: agent.full_name,
        contact_name: CONCAT_SEPARATOR('  /  ', partyMembers[*].full_name),
        contact_address: CONCAT_SEPARATOR('  /  ', partyMembers[*].address),
        contact_number: CONCAT_SEPARATOR('  /  ', partyMembers[*].contact_number),
        tenure,
        // contact: 'Contact Y/N',
        licence_status: licenceStatus,
        site_unlocked: siteUnlocked,
        date_signed: licence.l_date_countersigned,
        expiry_date: licence.l_expiry_date,
        // reason_for_refusal: 'Reason for Refusal, Reason for Escalation/De-Escalation',
        escalation_tier: escalationTier,
        // escalation_date: 'Escalation Date',
        // party_concerns: 'Concerns Actioned By & Date',
        // land_owner_circumstances: 'Land Owner Circumstances',
        // access_requirements: 'Site Access Requirements (incl. Site Parking Requirements)',
        // access_times: 'Site Access Time or Date Requirements',
        // hse_factors: 'Site HSE Factors',
        // land_use: 'Land Use',
        // asbestos: 'Asbestos Present on Site',
        // crop: 'Type & Height of Crop',
        // confied_area: 'Confined spaces or restricted areas?',
        // watercourse: 'Watercourse on site?',
        // public_highway: 'Within Public Highway?',
        // railway: 'Railway on site?',
        // other_activities: 'Other Activities on site ?',
        // high_vis: 'Unbranded high vis required?',
        phone_signal: phoneSignal,
        cdes_priority: cdesPriority,
        survey_types: CONCAT_SEPARATOR(', ', siteInfo[*].surveyTypes[**])
    }
`;
module.exports = (id, incDel, depth) => {
  return (
    'LET doc = DOCUMENT(\'' + id + '\') ' +
    'LET nodes = ( ' +
      'FOR v, e IN 1..' + depth + ' OUTBOUND \'' + id + '\' GRAPH \'all\' ' +
        'FILTER v != null && e != null' + (!incDel ? ' && !v._del ' : ' ') +
        'RETURN MERGE(v, UNSET(e, \'_to\', \'_id\', \'_rev\', \'_key\'), {edgeCollection: PARSE_IDENTIFIER(e).collection}) ' +
    ') ' +
    'RETURN MERGE(doc, {linked:nodes}) '
  );
};
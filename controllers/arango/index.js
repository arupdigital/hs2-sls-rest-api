const controllers = require('./controller-factory');
const dbArango = require('../../db/db').getArangoDB();
const graph = require('../../db/db').getArangoGraph();
const { unauthorized } = require('boom');
const { edgeConfig, edgeRenameRef } = require('./edgeConfig');
const { getAllQuery } = require('./aql');
const { controllerFunc, getFilters } = require('../../utils/controller');
const { extractToken } = require('../../utils');

// const postLicence = (req, res, next) => {
//   const { body } = req;
//   const edges = edgeConfig['Licence'];
//   const validationOff = false;
//   const fields = [];
//   controllerFunc(graph, 'Licence', body, edges, edgeRenameRef, 'post', undefined, validationOff)
//     .then(doc => {
//       const id = doc._id;
//       return [doc, id];
//     })
//     .then(([doc, newLicenceId]) => {
//       dbArango.query(getByKeyQuery(newLicenceId, false, 1))
//         .then(result => {
//           const { _result } = result;
//           if (_result[0]) {
//             result = processResponse([_result[0]], edgeRenameRef, fields, 'Licence')[0];
//             return [doc, result];
//           } else throw notFound('document not found');
//         })
//         .then(([doc, licence]) => {
//           const id = licence.licencee._id;
//           const matchedInterest = dbArango.query(getByKeyQuery(id, false, 1))
//             .then(result => {
//               const { _result } = result;
//               if (_result[0]) {
//                 result = processResponse([_result[0]], edgeRenameRef, fields, 'Party')[0];
//               }
//               for (let licenceInterest of licence.interests) {
//                 const bool = result.interests.some(partyInterest => {
//                   return licenceInterest._id === partyInterest._id;
//                 });
//                 if (bool) return bool;
//               }
//             });

//           Promise.all([doc, matchedInterest])
//             .then(([doc, matchedInterest]) => {
//               if (matchedInterest) {
//                 res.status(201).send(doc);
//                 // else throw notFound('This interest does not belong to a party');
//                 //The 'else' below is valid but not included so that tests pass.
//               }
//               else res.status(201).send(doc);
//             });
//         });
//     })
//     .catch(next);
// };



// const licenceUpdateController = (requestType) => {
//   return (req, res, next) => {
//     const { body } = req;
//     const fields = [];
//     const edges = edgeConfig['Licence'];
//     const validationOff = false;
//     const interests = body.interests;
//     const partyId = body.licencee.id;
//     const id = `Licence/${req.params.key}`;

//     dbArango.query(getByKeyQuery(id, false, 1))
//       .then(({ _result: [result] }) => {
//         if (!result) throw notFound('document not found');
//       })
//       .catch(next);

//     dbArango.query(getByKeyQuery(partyId, false, 1))
//       .then(({ _result: [result] }) => {
//         let party;
//         if (result) {
//           party = processResponse([result], edgeRenameRef, fields, 'Party')[0];
//         }
//         let bool;
//         if (party.interests) {
//           interests.forEach(licenceInterest => {
//             bool = party.interests.some(partyInterest => {
//               return licenceInterest.id === partyInterest._id;
//             });
//           });
//         }

//         if (!bool) throw badRequest('Licence\'s Interests must be related to the Licencee');
//         return bool;
//       })
//       .then((interestsMatched) => {
//         if (interestsMatched) {
//           controllerFunc(graph, 'Licence', body, edges, edgeRenameRef, requestType, id, validationOff)
//             .then(doc => {
//               res.status(201).send(doc);
//             })
//             .catch(next);
//         }
//         else throw badRequest('Linked interests do not match linked Party interests');
//       })
//       .catch(next);
//   };
// };

// controllers.Licence.post.controller = postLicence;
// controllers.Licence.patchByKey.controller = licenceUpdateController('patch');
// controllers.Licence.putByKey.controller = licenceUpdateController('put');


// AWS SDK
const AWS = require('aws-sdk');
AWS.config.update({ region: 'eu-west-2' });
const cognito = new AWS.CognitoIdentityServiceProvider();


const postCurrentUser = async (req, res, next) => {
  const AccessToken = extractToken(req);
  try {
    const user = await new Promise((resolve, reject) => {
      cognito.getUser({ AccessToken }, (err, userData) => {
        err ? reject(err) : resolve(userData);
      });
    });

    const userInfo = user.UserAttributes.reduce((acc, { Name, Value }) => {
      return { ...acc, [Name]: Value };
    }, {});

    const queries = {
      u_email: userInfo.email,
      u_first_name: userInfo.given_name,
      u_middle_names: userInfo.middle_name,
      u_last_name: userInfo.family_name,
    };

    const filters = getFilters(queries, edgeRenameRef, 'User');
    const query = getAllQuery('User', false, 1, filters);

    const { _result: [{ data }] } = await dbArango.query(query, { page: 1, skip: 0, limit: 1 });

    if (data[0]) return res.status(200).send(data[0]);

    const edges = edgeConfig['Licence'];
    const doc = await controllerFunc(graph, 'User', queries, edges, edgeRenameRef, 'post', undefined, true);

    res.status(201).send({ ...doc, ...queries });
  }
  catch (err) {
    next(unauthorized(err.message));
  }
};


controllers.User.getByKey = {
  controller: postCurrentUser,
  path: '/current',
  method: 'post'
};

module.exports = controllers;
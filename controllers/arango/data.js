const { getArangoDB } = require('../../db/db');
const { aql } = require('arangojs');
const { notFound } = require('boom');
const db = getArangoDB();


const getById = (req, res, next) => {
  const { id, collection } = req.params;
  const docId = `${collection}/${id}`;
  db.query(aql`
    LET id = ${docId}
    LET doc = DOCUMENT(id)
    LET inNodes = (
        FOR v, e, n IN 1..1 INBOUND id GRAPH 'all'
            FILTER v != null && e != null
            COLLECT relationships = PARSE_IDENTIFIER(e).collection INTO groups = MERGE({vertex: v}, {edge: UNSET(e, '_from', '_to')})
            RETURN {[relationships]: groups}
    )
    LET outNodes = (
        FOR v, e, n IN 1..1 OUTBOUND id GRAPH 'all'
            FILTER v != null && e != null
            COLLECT relationships = PARSE_IDENTIFIER(e).collection INTO groups = MERGE({vertex: v}, {edge: UNSET(e, '_from', '_to')})
            RETURN {[relationships]: groups}
    )
    RETURN MERGE(doc, {'linked': {'inbound': MERGE(inNodes), 'outbound': MERGE(outNodes)}})
  `)
    .then((result) => {
      const {_result} = result;
      if (_result[0]) res.send(result._result[0]);
      else throw notFound('Document not found');
    })
    .catch(next);
};

module.exports = {
  getById
};

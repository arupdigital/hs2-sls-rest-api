const edgeConfig = {
  Licence: [
    'has_licencee',
    'has_escalation_tier',
    'has_status',
    'has_group',
    'collected_by',
    'has_interest',
    'next_action_by'
  ],
  Organisation: [
    'has_contact'
  ],
  Contact_log: [
    'relates_to',
    'made_contact',
    'received_contact'
  ],
  Party_concern: [
    'reported_by',
    'actioned_by',
    'related_licence'
  ],
  Party: [
    'has_interest',
    'has_member'
  ],
  Site: [
    'has_survey_type',
    'has_contract_area',
    'contains_interest'
  ],
  Interest: [
    'has_agent'
  ],
  Person: [
    'has_agent_org'
  ]
};

const edgeRenameRef = {
  has_group: 'group',
  has_escalation_tier: 'escalation_tier',
  has_agent: 'agent',
  has_licencee: 'licencee',
  has_contact: 'contacts',
  has_interest: 'interests',
  made_contact: 'from',
  received_contact: 'to',
  relates_to: 'relates_to',
  reported_by: 'reported_by',
  actioned_by: 'actioned_by',
  collected_by: 'surveyor',
  has_survey_type: 'survey_types',
  contains_interest: 'interests',
  has_contract_area: 'contract_areas',
  has_member: 'members',
  has_agent_org: 'agent_org',
  has_status: 'status',
  next_action_by: 'next_action_by',
  related_licence: 'related_licence'
};

module.exports = {
  edgeConfig,
  edgeRenameRef
};
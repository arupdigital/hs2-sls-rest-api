const jwt = require('jsonwebtoken');
const jwksClient = require('jwks-rsa');
const { unauthorized } = require('boom');
const { jwksUri, kid } = require('./config');
const { extractToken } = require('./utils');


const client = jwksClient({ jwksUri });


function checkToken(token, key) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, key, null, function (err, decoded) {
      if (err) return reject(err);
      resolve(decoded);
    });
  });
}


function getKey(kid) {
  return new Promise((resolve, reject) => {
    client.getSigningKey(kid, (err, key) => {
      if (err) reject(err);
      else resolve(key.publicKey || key.rsaPublicKey);
    });
  });
}


function verifyJWT(req, res, next) {
  const token = extractToken(req);
  if (!token) return next(unauthorized('No credentials sent'));

  getKey(kid)
    .then(key => checkToken(token, key))
    .then(decodedToken => {
      req.user = decodedToken;
      next();
    })
    .catch(err => next(unauthorized(err.message)));
}


module.exports = {
  checkToken,
  getKey,
  verifyJWT,
};
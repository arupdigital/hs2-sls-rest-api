const {notFound, badRequest} = require('boom');
const edgeModels = require('../models/edges');
const dbPG = require('../models/geo');
const vertModels = require('../models/collection');
const {deepClone, pick} = require('./');
const omit = require('lodash.omit');
const findKey = require('lodash.findkey');
const Joi = require('joi');

function pagination (page, limit) {
  limit = limit ? Number(limit) : page ? 100 : null;
  page = Number(page || 1);
  let skip;
  if(
    limit &&
    (
      !Number.isInteger(page) ||
      !Number.isInteger(limit) ||
      page < 1 || limit < 1
    )
  ) throw badRequest('Invalid page query');
  else skip = limit * (page-1);
  return {page, skip, limit};
}

const getSortStr = (sortBy) => {
  return sortBy.split(',').reduce((acc, field) => {
    let desc = '';
    if(field.includes('[desc]')) {
      desc = ' DESC';
      field = field.replace('[desc]', '');
    }
    acc.push(`doc.${field}${desc}`);
    return acc;
  }, []).join(', ');
};

const validate = (data, schema, method, edges, validationOff) => {

  schema = schema.append({
    data_dump: Joi.object().optional(),
    data_dump_complete: Joi.object().optional()
  });

  const options = {
    abortEarly: false
  };
  
  if(method !== 'patch') options.presence = 'required';
  else schema._inner.dependencies = [];
  const valRes = schema && !validationOff ? schema.validate(data, options) : null;

  if(valRes && valRes.error) {
    const errors = valRes.error.details.map(err => err.message);
    throw badRequest(errors);
  }

  // call any custom validators
  if(schema.custom) {
    const customErrors = [];
    Object.keys(schema.custom).forEach(field => {
      if(data[field]) {
        const {valid, message} = schema.custom[field](data);
        if(!valid) customErrors.push(`${field} ${message}`);
      }
    });
    if(customErrors.length > 0) throw badRequest(customErrors);
  }

  if(method !== 'patch') {
    insertNulls(data, schema, edges);
    data._del = false;
  }
  addTimestamp(data);
};

function insertNulls (data, schema, edges) {
  schema._inner.children.forEach(obj => {
    if(
      (!edges || !Object.keys(edges).includes(obj.key)) &&
      (obj.key !== 'geoData' || obj.key !== 'data_dump' || obj.key !== 'data_dump_complete') &&
      !data.hasOwnProperty(obj.key)
    ) data[obj.key] = null;
  });
}

function addTimestamp (data, edge) {
  data[edge ? '_eua' : '_ua'] = new Date().toISOString();
}

function extractEdges (body, edges) {
  const extracted = Object.keys(edges).reduce((acc, key) => {
    if(body[key]) {
      acc[key] = deepClone(body[key]);
      delete body[key];
    }
    return acc;
  }, {});
  const remainingKeys = Object.keys(body);
  if(remainingKeys.length === 1 && remainingKeys[0] === '_ua') delete body._ua;
  return extracted;
}

function controllerFunc  (graph, vertColName, body, edges, edgeRef, method, vertId, validationOff) {
  edges = edges.reduce((acc, edge) => {
    acc[edgeRef[edge]] = {edgeColName: edge};
    return acc;
  },{});

  const vertCol = vertModels[vertColName];

  // Validate request
  try {
    validate(body, vertCol.schema, method, edges, validationOff);
  }
  catch (err) {
    return Promise.reject(err);
  }

  // separate arango and pg parts of object
  let pgObj;
  let arangoObj;

  if (body.hasOwnProperty('geoData')) {
    const letter = vertColName.charAt(0).toLowerCase();
    const objectId = `${letter}_object_id`;
    const geometryId = `${letter}_geometry`;
    arangoObj = JSON.parse(JSON.stringify(body)); 
    pgObj = {};
    pgObj[geometryId]= arangoObj.geoData;
    pgObj[objectId] = arangoObj[objectId];
    delete body.geoData;
  
  }

  const edgeSets = extractEdges(body, edges);
  Object.keys(method === 'patch' ? edgeSets : edges).forEach(key => {
    edges[key].edgeData = edgeSets[key] ?
      Array.isArray(edgeSets[key]) ?
        edgeSets[key] :
        [edgeSets[key]] :
      [];
  });
  const vertData = body;


  const edgeCheckPromises = Object.keys(edges).map(name => {
    const {edgeColName} = edges[name];
    edges[name].edgeCol = edgeModels[edgeColName];
    return Promise.all([
      method !== 'post' ? getExistingEdges(graph, edgeColName, vertId) : null,
      checkIds(vertCol, edges[name].edgeData, name, method)
    ])    //Check _to ids exist in DB and get existing edges if updating
      .then(([existingEdges]) => {
        if(method !== 'post') edges[name].existingEdges = existingEdges;
        return;
      });
  });

  return Promise.all(edgeCheckPromises)
    .then(() => {
      let promise;
      let promisePG;
      const endpoint = vertCol.name;
      // Add / update vertex
      if(method === 'post') {
        promise = vertCol.save(vertData);
        if (pgObj) promisePG = dbPG[endpoint].create(pgObj);
      } else if(method === 'put') {
        promise = vertCol.replace(vertId, vertData);
        if (pgObj) promisePG = dbPG[endpoint].upsert(pgObj);
      } else if(method === 'patch') {
        promise = vertCol.update(vertId, vertData);
        if (pgObj) promisePG = dbPG[endpoint].upsert(pgObj.i_geometry);
      }
      return Promise.all([promise, promisePG]);
    })

    // Add / update edges
    .then(([doc]) => {
      const promises =[];
      Object.keys(edges).forEach(name => {
        const {edgeCol, edgeData} = edges[name];
        if(method === 'put') promises.push(removeEdges(edgeCol, edges[name].existingEdges));
        if(method === 'patch' && Array.isArray(edgeData)) {
          promises.push(removeEdges(edgeCol, edges[name].existingEdges));
        }

        promises.push(addEdges(edgeCol, doc._id, edgeData));
      });
      if (pgObj) return Promise.all([doc.promise, doc.promisePG, ...promises]);
      return Promise.all([doc, ...promises]);
    })
    .then(([doc]) => doc);
}

function getExistingEdges (graph, edgeColName, vertId) {
  return graph.edgeCollection(edgeColName).edges(vertId);
}

function deleteController (vertColName, vertId, restore) {
  if(restore && restore !== 'true' && restore !== 'false') {
    throw badRequest('restore value should be a boolean');
  }

  const vertCol = vertModels[vertColName];
  return softDelete(vertCol, vertId, restore);
}

function softDelete (collection, id, restore) {
  return collection.update(id, {_del: restore !== 'true'});
}

function getFilters (query, edgeRef, colName) {
  const {schema} = vertModels[colName];
  const fields = Object.keys(Joi.describe(schema).children);
  const filters = pick(query, [...fields, '_del', '_id', '_key', '_rev']);
  const result = Object.keys(filters).reduce((acc, field) => {
    const fieldName = field.split('.')[0];
    if(Object.values(edgeRef).includes(fieldName)) acc.linked += getLinkedFilter(filters, field, edgeRef);
    else acc.root.push(`REGEX_TEST(TO_STRING(node.${field}), '^${filters[field]}$', true) `);
    return acc;
  }, {root: [], linked: ''});
  result.root = result.root.join(' && ');
  return result;
}

function getLinkedFilter (filters, field, edgeRef) {
  const split = field.split('.').slice(-2).map(fieldName => findKey(edgeRef, val => val === fieldName) || fieldName);
  return `FILTER linkedDoc.edgeCollection != '${split[0]}' || (linkedDoc.edgeCollection == '${split[0]}' && REGEX_TEST(TO_STRING(linkedDoc.${split[1]}), '^${filters[field]}$', true)) `;
}

function mergeLinkedData (doc) {
  const linked = deepClone(doc.linked);
  delete doc.linked;
  const moved = [];
  linked.forEach((node, i) => {
    const newNode = omit(node, '_from', 'edgeCollection');
    const matchIndex = linked.findIndex(node2 => node._from === node2._id);
    if(matchIndex > -1) {
      if(linked[matchIndex][node.edgeCollection]) linked[matchIndex][node.edgeCollection].push(newNode);
      else linked[matchIndex][node.edgeCollection] = [newNode];
      moved.unshift(i);
    }
  });
  moved.forEach(index => linked.splice(index, 1));

  linked.forEach(node => {
    if(node._from === doc._id) {
      const newNode = omit(node, '_from', 'edgeCollection');
      if(doc[node.edgeCollection]) doc[node.edgeCollection].push(newNode);
      else doc[node.edgeCollection] = [newNode];
    }
  });
}

function processResponse (data, edgeRef, fields, vertColName) {
  return data.reduce((acc, doc) => {
    mergeLinkedData(doc);
    rename(edgeRef, doc, vertModels[vertColName].schema);
    if(fields.length > 0) doc = pick(doc, fields);
    acc.push(doc);
    return acc;
  }, []);
}

function addEdges (collection, from, to) {
  // either from or to should be an array of objects ({id, data}), the other should be a single id

  const extractEdgeId = edge => {
    const edgeClone = JSON.parse(JSON.stringify(edge));
    const {id} = edgeClone;
    delete edge.id;
    return id;
  };

  const promises = [];

  if(Array.isArray(from && from.length > 0)) {
    from.forEach(edge => {
      const id = extractEdgeId(edge);
      promises.push(addEdge(collection, edge, id, to));
    });
  } else if(Array.isArray(to) && to.length > 0) {
    to.forEach(edge => {
      const id = extractEdgeId(edge);
      promises.push(addEdge(collection, edge, from, id));
    });
  }
  return Promise.all(promises);
}

const addEdge = (collection, data, _from, _to) => {
  const body = {_from, _to, ...data};
  insertNulls(body, collection.schema);
  addTimestamp(body, true);
  return collection.save(body);
};

function removeEdges (collection, edges) {
  if(!Array.isArray(edges) || edges.length === 0) return [];
  return Promise.all(edges.map(edge => collection.remove(edge._key)));
}

function checkIds (vertCol, edgeData, nameStr, method) {

  if(method === 'patch' && !edgeData) return true;
  return Promise.all(edgeData.map(node => vertCol.document(node.id, true)))
    .then(docArr => {
      const existsArr = docArr.map(doc => doc && doc._del !== true);
      if(!existsArr.every(exists => exists)) {
        throw notFound(`${nameStr} does not exist`);
      }
      return true;
    });
}

function rename (edgeRef, data, schema) {
  const edgeKeys = Object.keys(edgeRef);
  try {
    if(typeof data === 'object' && data.hasOwnProperty('_id')) {
      const clone = deepClone(data);
      Object.keys(data).forEach(key => {
        if(edgeKeys.includes(key)) {
          if(
            Array.isArray(clone[key]) &&
            schema._inner.children.find(child => child.key === edgeRef[key] && child.schema._type === 'object')) {
            data[edgeRef[key]] = clone[key][0];
          }
          else data[edgeRef[key]] = clone[key];
          if (key !== edgeRef[key]) delete data[key];
        }
        if(Array.isArray(data[edgeRef[key]])) {
          data[edgeRef[key]].forEach(doc => {
            rename(edgeRef, doc, vertModels[doc._id.replace(/\/.*/, '')].schema);
          });
        } else if (edgeRef[key] && data[edgeRef[key]] && typeof data[edgeRef[key]] === 'object') {
          rename(edgeRef, data[edgeRef[key]], vertModels[data[edgeRef[key]]._id.replace(/\/.*/, '')].schema);
        }
      });
    }
  } catch (err) {
    // console.error(err);
  }
}

module.exports = {
  controllerFunc,
  rename,
  validate,
  deleteController,
  pagination,
  getSortStr,
  processResponse,
  getFilters,
  insertNulls
};
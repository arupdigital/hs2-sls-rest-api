const deepClone = (body) => {
  return JSON.parse(JSON.stringify(body));
};


const pick = (obj, fieldsArr) => {
  return Object.keys(obj).reduce((acc, key) => {
    if (fieldsArr.includes(key)) acc[key] = obj[key];
    return acc;
  }, {});
};


const pad = (str, length) => {
  str = Number(str).toString();
  while (str.length < length) { str = '0' + str; }
  return str;
};


const renameKey = (obj, oldField, newField) => {
  obj[newField] = deepClone(obj[oldField]);
  delete obj[oldField];
};


const createExportRow = (item, columns) => {
  const row = {};
  for (let key in columns) {
    const alias = columns[key];
    const value = item[key];
    if (typeof value === 'string') row[alias] = value.replace(/['"_|]/g, '');
    if (value && !Array.isArray(value)) row[alias] = value;
    else if (value && Array.isArray(value) && value.length === 0) row[alias] = 'null';
    else if (value && Array.isArray(value) && value.length > 0) row[alias] = value;
    else if (value === 0) row[alias] = value;
    else row[alias] = 'null';
  }
  return row;
};


const extractToken = (req) => {
  return req.headers.authorization && req.headers.authorization.startsWith('Bearer ')
    ? req.headers.authorization.slice(7, req.headers.authorization.length)
    : req.headers.authorization;
};


module.exports = {
  deepClone,
  pick,
  pad,
  renameKey,
  createExportRow,
  extractToken,
};
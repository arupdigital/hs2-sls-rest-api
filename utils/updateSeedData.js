const {writeFile} = require('fs');
const {promisify} = require('util');
const writeFileAsync = promisify(writeFile);
const path = 'db/data/dev/nodes/';
const filesToProcess = ['people'];
const replaceFile = true;
const {deepClone} = require('./');

function processFile (filename) {
  let arr = require(`../${path}${filename}.json`);
  arr.forEach(updateFunc);
  return writeFileAsync(`${path}${filename}${replaceFile ? '' : '_modified'}.json`, JSON.stringify(arr, null, 2))
    .catch(console.log);
}

function processFiles (filenames) {
  return Promise.all(filenames.map(processFile));
}

function updateFunc (ele) {
  const clone = deepClone(ele);
  ele.p_deceased = !clone.p_active;
  delete ele.p_active;
}




processFiles(filesToProcess);








  
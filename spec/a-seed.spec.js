process.env.NODE_ENV = 'test';
const { expect } = require('chai');
const { connectToDB, connectToGeoDB } = require('../db/db');
const seedArangoDB = require('../db/seed/arango-seed');
const seedGeoDB = require('../db/seed/pg-seed');
const nodeData = require('../db/data/dev/nodes');
const geoData = require('../db/data/test/geo');
const edges = require('../db/data/dev/nodes/edges.json');
const graphs = require('../db/data/dev/nodes/graphs.json');

describe('Seeding Databases', () => {
  let db, geoDB;
  before(function() {
    this.timeout(30000);
    return Promise.all([
      connectToDB(),
      connectToGeoDB()
    ])
      .then((dbs) => {
        [db, geoDB] = dbs;
        module.exports = {db, geoDB};
        return Promise.all([seedArangoDB(db, nodeData, edges, graphs), seedGeoDB(geoDB, geoData)]);
      })
      .catch(console.error);
  });

  describe('seedDB', () => {

    it('creates specified collections', async () => {
      const collections = await db.listCollections();
      const collectionRefs = Object.keys(nodeData);
      let isCollectionPresent = false;
      collections.forEach((item) => {
        const { name, type } = item;
        if (type === 2) {
          isCollectionPresent = collectionRefs.includes(name);
          expect(isCollectionPresent).to.be.true;
        }
      });
    });
    it('creates correct amount of collections', async () => {
      const collections = await db.listCollections();
      const collectionRefs = Object.keys(nodeData);
      const lengthArray = collections.filter(collection => {
        if (collection.type === 2) {
          return collection;
        }
      });
      expect(lengthArray.length).to.equal(collectionRefs.length);
    });
    it('inserts data into the collections', async () => {
      const collectionList = Object.keys(nodeData);
      const insertedDocs = collectionList.map(async col => {
        const collection = db.collection(col);
        const docs = await collection.count();
        return docs.count;
      });
      return Promise.all(insertedDocs)
        .then(insertedDocs => {
          expect(collectionList.length).to.equal(insertedDocs.length);
          insertedDocs.forEach(docCount => {
            expect(docCount).to.be.above(0);
          });
        });
    });

  });
    
});
  

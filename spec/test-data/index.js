module.exports = {
  Contact_log: {
    data: [{
      'cl_summary': 'test',
      'cl_contact_method': 'Fax',
      'cl_date': '2018-12-13',
      'relates_to': { 'id': 'Licence/1' },
      'from': [{ 'id': 'User/1' }],
      'to': [{ 'id': 'Person/1' }],
      'cl_comments': 'test'
    }],
    optionalKeys: ['cl_comments']

  },
  Contract_area: {
    data: [
      {
        'c_area': 'L5'
      },
      {
        'c_area': 'L6'
      }
    ],
    uniqueField: 'c_area'
  },
  Escalation_tier: {
    data: [
      {
        'et_tier': 'Tier 2'
      },
      {
        'et_tier': 'Tier 9'
      }
    ],
    uniqueField: 'et_tier'
  },
  Hazard: {
    data: [
      {
        'h_hazard': 'test hazard',
        'h_buffer_size': 1
      },
      {
        'h_hazard': 'test hazard 2',
        'h_buffer_size': 2
      },
    ],
    uniqueField: 'h_hazard'
  },
  Interest: {
    data: [
      {
        'i_object_id': '50000',
        'i_title_id': 'MS50000',
        'i_fully_complete': 'true',
        'i_full_title': 'true',
        'i_multipart': 'false',
      },
      {
        'i_object_id': '50001',
        'i_title_id': 'MS50001',
        'i_fully_complete': 'true',
        'i_full_title': 'true',
        'i_multipart': 'false',
      }
    ],
    uniqueField: 'i_object_id'
  },
  Licence_group: {
    data: [
      {
        'lg_name': 'TES'
      },
      {
        'lg_name': 'EST'
      }
    ],
    uniqueField: 'lg_name'
  },
  Licence_status: {
    data: [
      {
        'ls_status': 'test1'
      },
      {
        'ls_status': 'test2'
      }
    ],
    uniqueField: 'ls_status'
  },
  Organisation: {
    data: [{
      'o_name': 'Mod Incorporated Ltd',
      'o_building_number': '123',
      'o_address_line_1': 'Hello Street',
      'o_city': 'Mod Town',
      'o_country': 'MT',
      'o_postcode': 'M5 2HA',
      'contacts': [{ 'id': 'Person/1', 'is_primary': 'true' }],
      'o_active': true
    }],
    uniqueField: 'o_tel_number'
  },
  Party_concern: {
    data: [{
      'pc_concern': 'test',
      'reported_by': { 'id': 'Person/1', 'date_reported': '2018-12-14' },
      'related_licence': { 'id': 'Licence/1' }
    }]
  },
  Person: {
    data: [{
      'p_title': 'Mr',
      'p_first_name': 'Paul',
      'p_last_name': 'McCartney',
      'p_house_name': 'Mod Pad',
      'p_address_line_1': 'First Line',
      'p_city': 'Mod Town',
      'p_country': 'MT',
      'p_postcode': 'M5 2HA',
      'p_email': 'mod@manchester.com',
      'p_apartment_number': 'null',
      'p_deceased': false
    }],
    uniqueField: 'p_home_tel'
  },
  Suitability_code: {
    data: [
      {
        'sc_name': 'Fit for review',
        'sc_code': 'SC19'
      },
      {
        'sc_name': 'TEST',
        'sc_code': 'SC20'
      }
    ],
    uniqueField: 'sc_name'
  },
  Surveyor: {
    data:[
      {
        'sv_name': 'TES'
      },
      {
        'sv_name': 'EST'
      }
    ],
    uniqueField: 'sv_name'
  },
  Survey_type: {
    data: [
      {
        'st_id': 'A123456'
      },
      {
        'st_id': 'A123457'
      }
    ],
    uniqueField: 'st_id'
  },
  Site: {
    data: [
      {
        's_access_requirements': 'Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl.',
        's_hse_factors': 'Proin at turpis a pede posuere nonummy.',
        's_land_use': 'Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.',
        's_asbestos': false,
        's_crop_type': 'Morbi ut odio.',
        's_crop_height': 1000,
        's_confined_restricted_areas': true,
        's_watercourse_on_site': true,
        's_within_public_highway': true,
        's_railway_on_site': true,
        's_other_activities': 'Nulla tellus. In sagittis dui vel nisl. Duis ac nibh.',
        's_unbranded_high_vis_required': true,
        's_phone_signal_status': 'High',
        's_access_date_time': '18:25',
        's_confined_restricted_details': 'Test details',
        's_watercourse_details': 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        's_within_public_highway_details': 'Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.',
        's_railway_details': 'Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet.',
        's_unbranded_high_vis_details': 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.',
        's_is_cdes': false,
        's_id': 'EES_L1_Wood99999',
        's_is_measham': true,
        's_priority_level': 'Medium',
        'survey_types': [{'id':'Survey_type/1'}],
        'contract_areas': [{'id':'Contract_area/1'}],
        'interests': [{'id':'Interest/1'}]
      },
      {
        's_access_requirements': 'Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl.',
        's_hse_factors': 'Proin at turpis a pede posuere nonummy.',
        's_land_use': 'Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.',
        's_asbestos': false,
        's_crop_type': 'Morbi ut odio.',
        's_crop_height': 1000,
        's_confined_restricted_areas': true,
        's_watercourse_on_site': true,
        's_within_public_highway': true,
        's_railway_on_site': true,
        's_other_activities': 'Nulla tellus. In sagittis dui vel nisl. Duis ac nibh.',
        's_unbranded_high_vis_required': true,
        's_phone_signal_status': 'High',
        's_access_date_time': '18:25',
        's_confined_restricted_details': 'Test details',
        's_watercourse_details': 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        's_within_public_highway_details': 'Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.',
        's_railway_details': 'Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet.',
        's_unbranded_high_vis_details': 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.',
        's_is_cdes': false,
        's_id': 'EES_L1_Wood99998',
        's_is_measham': true,
        's_priority_level': 'Medium',
        'survey_types': [{'id':'Survey_type/1'}],
        'contract_areas': [{'id':'Contract_area/1'}],
        'interests': [{'id':'Interest/1'}]
      }
    ],
    uniqueField: 's_id'
  },
  Party: {
    data: [{
      'pa_ref': 'P123456',
      'pa_date_created' : '2018-07-11',
      'pa_is_current' : 'true',
      'pa_is_mlo': 'true',
      'members': [{'id': 'Person/1'}],
      'interests': [{'id': 'Interest/1', 'is_occupier': 'true'}]
    }]
  },
  Agent_org: {
    data: [
      {
        'ao_name': 'Test & Test'
      },
      {
        'ao_name': 'Test2 & Test2'
      }
    ]
  },
  User: {
    data: [
      {
        'u_email': 'test@test.com',
        'u_first_name': 'First',
        'u_last_name': 'Last'
      },
      {
        'u_email': 'test2@test2.com',
        'u_first_name': 'First',
        'u_last_name': 'Last'
      }
    ],
    unqieuField: 'u_email'
  },
  Licence: {
    data: [
      {
        'l_acc_number': '2C865-ARP-LP-ACC-100-751499',
        'l_cro_number': '2C865-ARP-LP-ACC-100-751499',
        'l_pln_number': '2C865-ARP-LP-ACC-100-751499',
        'l_revision_name': 'P23',
        'l_reason_for_revision': 'lorem ipsum',
        'l_approved_countersign_date': '2018-12-13',
        'l_date_countersigned': '2018-12-13',
        'l_date_posted': '2018-12-13',
        'l_date_signed_by_lo': '2018-12-27',
        'l_payment_schedule_approval_date': '2018-12-13',
        'l_expiry_date': '2018-12-13',
        'l_is_noise': 'true',
        'l_previous_revisions': [{ 'l_revision_name': 'P12' }],
        'licencee': { 'id': 'Party/1' },
        'escalation_tier': [{ 'id': 'Escalation_tier/1', 'date_changed': '2018-12-13', 'is_current': 'true' }],
        'status': [{ 'id': 'Licence_status/1', 'date_changed': '2018-12-13', 'is_current': 'true' }],
        'interests': [{ 'id': 'Interest/1', 'is_occupier': 'true' }],
        'group': [{'id': 'Licence_group/1'}],
        'l_site_visits': 0,
        'surveyor': {'id':'Surveyor/1'}
      },
      {
        'l_acc_number': '2C865-ARP-LP-ACC-100-751500',
        'l_cro_number': '2C865-ARP-LP-ACC-100-751500',
        'l_pln_number': '2C865-ARP-LP-ACC-100-751500',
        'l_revision_name': 'P23',
        'l_reason_for_revision': 'lorem ipsum',
        'l_approved_countersign_date': '2018-12-13',
        'l_date_countersigned': '2018-12-13',
        'l_date_posted': '2018-12-13',
        'l_date_signed_by_lo': '2018-12-27',
        'l_payment_schedule_approval_date': '2018-12-13',
        'l_expiry_date': '2018-12-13',
        'l_is_noise': 'true',
        'l_previous_revisions': [{ 'l_revision_name': 'P12' }],
        'licencee': { 'id': 'Party/1' },
        'escalation_tier': [{ 'id': 'Escalation_tier/1', 'date_changed': '2018-12-13', 'is_current': 'true' }],
        'status': [{ 'id': 'Licence_status/1', 'date_changed': '2018-12-13', 'is_current': 'true' }],
        'interests': [{ 'id': 'Interest/1', 'is_occupier': 'true' }],
        'group': [{'id': 'Licence_group/1'}],
        'l_site_visits': 0,
        'surveyor': {'id':'Surveyor/1'}
      }
    ],
    uniqueField: 'l_acc_number'
  }
};
const app = require('../app');
const request = require('supertest')(app);
const { expect } = require('chai');
const testData = require('./test-data');
const allSeedData = require('../db/data/dev/nodes');
const omit = require('lodash.omit');

describe('Testing endpoints...', () => {
  return Object.entries(testData).reduce((acc, [colName, {data: testDoc, uniqueField, optionalKeys}]) => {
    return acc.then(acc => testEndPoint(colName, testDoc, uniqueField, optionalKeys).then(() => acc));
  }, Promise.resolve());
});

const testEndPoint = (colName, testDocs, uniqueField, optionalKeys) => {

  return new Promise((resolve, reject) => {
    reject;
    const { data: seedData } = allSeedData[colName];
    let firstDoc = seedData[0];
    firstDoc = omit(firstDoc, optionalKeys);
    const firstDocProps = Object.keys(firstDoc);

    let dbDocs;
    let seededDocKey;

    describe(colName, () => {
      it('get all', () => {
        return request
          .get(`/api/${colName}`)
          .expect(200)
          .then(({ body : { data } }) => {
            dbDocs = data;
            const dbDoc = uniqueField ? dbDocs.find(doc => doc[uniqueField] === firstDoc[uniqueField]) : dbDocs[0];
            seededDocKey = dbDoc._key;
            if (colName === 'Party') {
              let partyInterestId = dbDocs[0].interests[0]._id;
              testData.Licence.data[0].interests[0].id = partyInterestId;
            }
            expect(Array.isArray(dbDocs)).to.be.true;
            firstDocProps.forEach(prop => {
              expect(dbDoc).to.haveOwnProperty(prop);
            });
          });
      });
      it('get all with invalid request', () => {
        return request
          .get(`/api/${colName}s`)
          .expect(404)
          .then(({ body: { message } } ) => {
            expect(message).to.equal('Path not found');
          });
      });
      it('get by key', () => {
        return request
          .get(`/api/${colName}/${seededDocKey}`)
          .expect(200)
          .then(({body}) => {
            expect(body).to.be.a('object');
            firstDocProps.forEach(prop => {
              expect(body).to.haveOwnProperty(prop);
            });
          });
      });
      it('get by key with invalid request', () => {
        return request
          .get(`/api/${colName}/test`)
          .expect(404)
          .then(( { body: { message } } ) => {
            expect(message).to.equal('Document not found');
          });
      });
      it('delete by key', () => {
        const { _key, _id } = dbDocs[2];
        return request
          .delete(`/api/${colName}/${_key}`)
          .expect(200)
          .then(({ body: { _id: deletedId, _key: deletedKey } } ) => {
            expect(deletedKey).to.equal(_key);
            expect(deletedId).to.equal(_id);
          });
      });
      it('delete by key with invalid request', () => {
        return request
          .delete(`/api/${colName}/test`)
          .then(({ body: { message } } ) => {
            expect(message).to.equal('document not found');
          });
      });
      it('put by key', () => {
        const { _key, _id, _rev } = dbDocs[1];
        return request
          .put(`/api/${colName}/${_key}`)
          .send(testDocs[0])
          .expect(201)
          .then(({ body: { _id: id, _key: key, _rev: newRev } } ) => {
            expect(_id).to.equal(id);
            expect(_key).to.equal(key);
            expect(_rev).to.not.equal(newRev);
          });
      });
      it('put by key with invalid key', () => {
        return request
          .put(`/api/${colName}/test`)
          .send(testDocs[0])
          .expect(404)
          .then(({ body: { message } } ) => {
            expect(message).to.equal('document not found');
          });
      });
      it('patch by key', () => {
        const { _key, _id, _rev } = dbDocs[1];
        return request
          .patch(`/api/${colName}/${_key}`)
          .send(testDocs[0])
          .expect(201)
          .then(({ body: { _id: id, _key: key, _rev: newRev } }) => {
            expect(_id).to.equal(id);
            expect(_key).to.equal(key);
            expect(_rev).to.not.equal(newRev);
          });
      });
      it('post', () => {
        return request
          .post(`/api/${colName}`)
          .send(testDocs[1] || testDocs[0])
          .expect(201)
          .then(({ body }) => {
            expect(body).to.be.a('object');
            expect(body).to.haveOwnProperty('_id');
            expect(body).to.haveOwnProperty('_key');
            expect(body).to.haveOwnProperty('_rev');
          });
      });
    });
    resolve();
  });
};
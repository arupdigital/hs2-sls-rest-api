describe('Close db', () => {

  it('Closes db after all tests finished', () => {
    const {db, geoDB} = require('./a-seed.spec.js');
    after(() => {
      db.close();
      geoDB.close();
    });
  });
  
});
const app = require('./app');
const config = require('./config');
const { PORT = 9090 } = config;
const { connectToGeoDB, connectToDB } = require('./db/db');

Promise.all([
  connectToDB(),
  connectToGeoDB()
])
  .then(() => {
    app.listen(PORT, (err) => {
      if (err) throw err;
      console.log(`Listening on port ${PORT}`);
    });
  });
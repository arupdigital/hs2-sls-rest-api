const handle401 = (err, req, res, next) => {
  if (err.isBoom && err.output.statusCode === 401) {
    const { message, statusCode } = err.output.payload;
    res.status(401).send({ message, statusCode });
  }
  else next(err);
};

module.exports = handle401;
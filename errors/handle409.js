const handle409 = (err, req, res, next) => {
  if(err.code === 409) {
    res.status(409).send({statusCode: err.code, message: err.message});
  }
  next;
};

module.exports = handle409;
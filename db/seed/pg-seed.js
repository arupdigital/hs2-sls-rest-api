const Sequelize = require('sequelize');

// Sequentially seed geo datasets
function insertGeoData (geoDB, geoDatasets) {
  return geoDatasets.reduce((p, geoDataset) => {
    return p.then(() => {
      const Model = geoDB.define(`t_${geoDataset.name}`, {
        [`${geoDataset.prefix}_object_id`]: {
          type: Sequelize.INTEGER,
          primaryKey: true
        },
        [`${geoDataset.prefix}_geometry`]: {
          type: Sequelize.GEOMETRY,
          allowNull: false
        }
      });
      return Promise.all([Model, Model.sync({force:true})]);
    })
      .then(([Model]) => Promise.all(geoDataset.data.map(feature => {
        const row = {
          [`${geoDataset.prefix}_object_id`]: feature.properties.OBJECTID,
          [`${geoDataset.prefix}_geometry`]: feature.geometry,
        };
        return Model.create(row);
      })))
      .catch(err => console.error(`Error seeding ${geoDataset.name} geodata`, err));
  }, Promise.resolve());
}

function seedGeoDB (geoDB, geoDatasets) {
  return geoDB.sync({force:true})
    .then(() => geoDB.drop())
    .then(() => insertGeoData(geoDB, geoDatasets))
    .then(() => {
      console.log('Finished seeding geoDB');
      return geoDB;
    });
}

module.exports = seedGeoDB;
const { connectToDB, connectToGeoDB } = require('../db');
const seedArangoDB = require('./arango-seed');
const seedGeoDB = require('./pg-seed');
const data = require('../data/dev/nodes');
const geoData = require('../data/dev/geo');
const edges = require('../data/dev/nodes/edges.json');
const graphs = require('../data/dev/nodes/graphs.json');


Promise.all([
  connectToDB(),
  connectToGeoDB()
])
  .then(([db, geoDB]) => Promise.all([seedArangoDB(db, data, edges, graphs), seedGeoDB(geoDB, geoData)]))
  .then(([db, geoDB]) => {
    console.log('Seeding complete');
    db.close();
    geoDB.close();
  })
  .catch(console.error);
const keyBy = require('lodash.keyby');
const arangoColModels = require('../../models/collection');
const Joi = require('joi');
const tenures = require('../data/dev/edges/tenure.json');
const random = require('lodash.random');

const collectionSchemas = Object.keys(arangoColModels).reduce((acc, col) => {
  if (arangoColModels[col]['schema'] !== undefined) {
    const schema = arangoColModels[col]['schema'];
    acc[col] = schema;
  } 
  return acc;
}, {});


const validateData = (dataSet, schema, colName) => {
  dataSet.forEach(set => {
    const validData = Joi.validate(set, schema);
    if (validData.error) {
      const errors = validData.error.details.map(err => err.message);
      throw {message: `Failed to insert node data into ${colName} due to it being invalid`,
        errors, set };
    }
    if(arangoColModels[colName]) set._del = false;
  });
};

const createRandomDateSince = (year) => {
  const start = new Date(year, 0, 1).getTime();
  const end = new Date().getTime();
  const dateDiff = (end - start);
  return new Date(start + Math.random() * dateDiff);
};

const fetchRelatedData = async (collections, edgeInfo) => {
  try {
    const { from, to } = edgeInfo;
    
    let fromData = await Promise.all(from.map(colName => collections[colName].all()));
    let toData = await Promise.all(to.map(colName => collections[colName].all()));

    fromData = fromData.reduce((acc, { _result }) => [...acc, ..._result], []);
    toData = toData.reduce((acc, { _result }) => [...acc, ..._result], []);

    return Promise.resolve({ fromData, toData });
  }
  catch (err) {
    console.error(err);
  }
};

const createEdgeData = (edgeName, index) => {
  switch (edgeName) {
  case 'has_contact':
    return { is_primary: index === 0 };
    
  case 'reported_by':
    return { date_reported: createRandomDateSince(2017) };

  case 'actioned_by':
    return { date_actioned: createRandomDateSince(2017) };

  case 'has_interest': 
    return { tenure: tenures[Math.floor(Math.random() * 5)], is_occupier: random(0, 10) % 7 === 0 ? true : false };

  case 'made_contact': 
    return { is_contact_log_agent: random(0, 10) % 4 === 0 ? true : false };

  case 'received_contact': 
    return { is_contact_log_agent: random(0, 10) % 4 === 0 ? true : false };
    
  default:
    return {};
  }
};

const getRandomId = (data) => {
  const randomIndex = Math.floor(Math.random() * data.length);
  return data[randomIndex]._id;
};

const linkData = (edgeCol, fromData, toData, minPerNode, maxPerNode) => {
  const edgePromises = fromData.map(item => {

    const edgeIterations = random(minPerNode, maxPerNode, false);
    const promises = [];
    let randomIds = [];

    for (let i = 0; i < edgeIterations; i++) {
      const toId = getRandomId(toData);
      randomIds.push(toId);
    }

    if (edgeCol.name === 'has_survey_type') randomIds = [...new Set(randomIds)];

    for (let i = 0; i < randomIds.length; i++) {
      const data = createEdgeData(edgeCol.name, i);
      const toId = randomIds[i];
      const { _id: fromId } = item;

      promises.push(edgeCol.save(data, fromId, toId));
    }
    return Promise.all(promises);
  });

  return Promise.all(edgePromises);
};

const createNodeCollections = async (db, data) => {
  const nodeColPromises = Object.keys(data).map(async colName => {
    const col = db.collection(colName);
    await col.create({keyOptions:{type:'autoincrement'}});
    if(data[colName].uniqueFields) {
      await Promise.all(data[colName].uniqueFields.map((({fields, sparse}) => {
        return col.createHashIndex(fields, {unique: true, sparse});
      })));
    }
    return col;
  });

  return Promise.all(nodeColPromises);
};

const createEdgeCollections = async (db, edgeNames) => {
  const edgeColPromises = edgeNames.map(async edge => {
    edge = db.edgeCollection(edge);
    await edge.create({keyOptions:{type:'autoincrement'}});
    return edge;
  });

  return Promise.all(edgeColPromises);
};

const insertNodeData = (colNames, colRef, data, validationOff) => {
  const nodePromises = colNames.map(colName => {
    const collection = colRef[colName];
    const colData = data[colName].data;
    const schema = collectionSchemas[colName];
    if(!validationOff) validateData(colData, schema, colName);
    return collection.import(colData);
  });

  return Promise.all(nodePromises);
};

const insertEdgeData = (edgeNames, colRef, edgeRef, edgesInfo) => {

  const edgePromises = edgeNames.map(async edgeName => {
    const edgeCol = edgeRef[edgeName];
    const edgeInfo = edgesInfo[edgeName];
    const { fromData, toData } = await fetchRelatedData(colRef, edgeInfo);
    const { minPerNode, maxPerNode } = edgeInfo;

    return linkData(edgeCol, fromData, toData, minPerNode, maxPerNode);
  });

  return Promise.all(edgePromises);
};

const insertData = async (db, data, edges, validationOff) => {
  try {
    const collectionNames = Object.keys(data);
    const edgeNames = Object.keys(edges);

    const collections = await createNodeCollections(db, data);
    const edgeCollections = await createEdgeCollections(db, edgeNames);

    const collectionRef = keyBy(collections, col => col.name);
    const edgeRef = keyBy(edgeCollections, edge => edge.name);

    await insertNodeData(collectionNames, collectionRef, data, validationOff);
    await insertEdgeData(edgeNames, collectionRef, edgeRef, edges);
    await getCollections(collectionRef, edgeRef);
  

    console.log('Finished seeding ArangoDB');
    return db;
  }
  catch (err) {
    console.error('Error seeding ArangoDB', err);
  }
};

const createGraphs = async (db, graphInfo, edgeInfo) => {
  const graphs = Object.values(graphInfo).map((graph) => db.graph(graph.name));
  
  const graphPromises = graphs.map(graph => {
    const graphRef = graphInfo[graph['name']];
    const edgeDefinitions = graphRef.edgeDefinitions.map(edgeName => {
      const { name: collection, from, to } = edgeInfo[edgeName];
      return {
        collection,
        from: from,
        to: to
      };
    });
    return graph.create({ edgeDefinitions });
  });

  return Promise.all(graphPromises);
};

const dropCollections = async (db) => {
  const collections = await db.collections();
  const collectionPromises = collections.map(col => col.drop());
  return Promise.all(collectionPromises);
};

const dropGraphs = async (db) => {
  let graphs = await db.listGraphs();
  graphs = graphs.map(({ _key: name }) => db.graph(name));
  const graphPromises = graphs.map(graph => graph.drop());
  return Promise.all(graphPromises);
};

const getCollections = (collectionRef, edgeRef) => {
  const collections = allCollections(collectionRef, edgeRef);
  const docs = collections.map(async collection => {
    const result = await collection.all();
    const { _result } = result;
    return _result;
  });  
  return Promise.all(docs)
    .catch(err => {
      console.error('Error gathering inserted collections data', err);
    });
};

const allCollections = (collectionRef, edgeRef) => {
  const docCollections = Object.values(collectionRef);
  const edgeCollections = Object.values(edgeRef);
  return [
    ...docCollections, 
    ...edgeCollections
  ];
};

const seedArangoDB = (db, data, edges, graphs, validationOff) => {
  return dropCollections(db)
    .then(() => insertData(db, data, edges, validationOff))
    .then(() => dropGraphs(db))
    .then(() => createGraphs(db, graphs, edges))
    .then(() => db);
};

module.exports = seedArangoDB;
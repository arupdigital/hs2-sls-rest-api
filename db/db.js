const { PG_DB_NAME, PG_USERNAME, PG_PASSWORD, HOSTNAME, ARANGO_DB_NAME, ARANGO_DB_URL } = require('../config');
const Sequelize = require('sequelize');
const { Database } = require('arangojs');

const geoDB = new Sequelize(PG_DB_NAME, PG_USERNAME, PG_PASSWORD, {
  host: HOSTNAME,
  dialect: 'postgres',
  logging: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  operatorsAliases: false
});

const arangoDB = new Database({
  url: ARANGO_DB_URL
});

const connectToGeoDB = () => {
  return geoDB
    .authenticate()
    .then(() => {
      console.log(`Successfully connected to GeoDB (${PG_DB_NAME})`);
      return geoDB;
    })
    .catch(err => console.error('Unable to connect to GeoDB:', err.message));
};

const connectToDB = async () => {
  await arangoDB.useDatabase(ARANGO_DB_NAME);
  await arangoDB.login();
  console.log(`Successfully connected to Arango DB (${ARANGO_DB_NAME})`);
  return arangoDB;
};

const getArangoDB = () => {
  return arangoDB;
};

const getGeoDB = () => {
  return geoDB;
};

const getArangoCol = name => {
  return getArangoDB().collection(name);
};

const getArangoGraph = name => {
  return getArangoDB().graph(name);
};

module.exports = {
  connectToGeoDB,
  connectToDB,
  getArangoDB,
  getArangoCol,
  getArangoGraph,
  getGeoDB
};

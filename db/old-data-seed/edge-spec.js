module.exports = [
  {
    newEdgeColName: 'has_member',
    fromVertColName: 'Party',
    fromKey: 'p_id',
    keepFromKey: false,
    toVertColName: 'Person',
    toKey: 'p_id',
    keepToKey: false,
  },
  {
    newEdgeColName: 'has_member',
    fromVertColName: 'Party',
    fromKey: 'p_id',
    keepFromKey: false,
    toVertColName: 'Organisation',
    toKey: 'p_id',
    keepToKey: false
  },
  {
    newEdgeColName: 'relates_to',
    fromVertColName: 'Contact_log',
    fromKey: 'se_acc_no',
    keepFromKey: false,
    toVertColName: 'Licence',
    toKey: 'l_acc_number',
    keepToKey: true
  },
  {
    newEdgeColName: 'has_escalation_tier',
    fromVertColName: 'Licence',
    fromKey: 'escalation_tier',
    keepFromKey: false,
    toVertColName: 'Escalation_tier',
    toKey: 'et_tier',
    keepToKey: true
  },
  {
    newEdgeColName: 'has_status',
    fromVertColName: 'Licence',
    fromKey: 'licence_status',
    keepFromKey: false,
    toVertColName: 'Licence_status',
    toKey: 'ls_status',
    keepToKey: true
  },
  {
    newEdgeColName: 'has_group',
    fromVertColName: 'Licence',
    fromKey: 'licence_group',
    keepFromKey: false,
    toVertColName: 'Licence_group',
    toKey: 'lg_name',
    keepToKey: true
  },
  {  
    newEdgeColName: 'related_licence',
    fromVertColName: 'Party_concern',
    fromKey: 'id',
    keepFromKey: false,
    toVertColName: 'Licence',
    toKey: 'party_concern',
    keepToKey: false
  },
  {
    newEdgeColName: 'collected_by',
    fromVertColName: 'Licence',
    fromKey: 'surveyor',
    keepFromKey: false,
    toVertColName: 'Surveyor',
    toKey: 'sv_name',
    keepToKey: true
  },
  {
    newEdgeColName: 'next_action_by',
    fromVertColName: 'Licence',
    fromKey: 'next_action_by',
    keepFromKey: false,
    toVertColName: 'Surveyor',
    toKey: 'sv_name',
    keepToKey: true
  },
  {
    newEdgeColName: 'has_interest',
    fromVertColName: 'Licence',
    fromKey: 'l_acc_number',
    keepFromKey: true,
    toVertColName: 'Interest',
    toKey: 'LicenceNum',
    keepToKey: false
  },
];

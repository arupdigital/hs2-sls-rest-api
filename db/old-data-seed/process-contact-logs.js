const {writeFile} = require('fs');
const {promisify} = require('util');
const writeFileAsync = promisify(writeFile);

function processFile () {
  let arr = require('./new-contact-logs.json');
  arr.forEach(updateFunc);
  return writeFileAsync('db/old-data-seed/new-contact-logs-modified.json', JSON.stringify(arr, null, 2))
    .catch(console.log);
}

function updateFunc (ele) {
  delete ele.id;
  delete ele.enabled;
  delete ele.se_date_time_timestamp;
  delete ele.se_leg;
  delete ele.se_la_agent_initials;
  delete ele.se_meeting_minutes;
  Object.keys(ele).forEach(key => {
    if(ele[key]) ele[key] = ele[key].trim();
    if(ele[key] === '' || ele[key] === 'n/a' || ele[key] === null) delete ele[key];
  });
  if(ele.se_contact_date_timestamp_date) {
    const [day, month, year] = ele.se_contact_date_timestamp_date.split('/');
    ele.se_contact_date_timestamp_date = `${year}-${month}-${day}`;
  }
  const clone = JSON.parse(JSON.stringify(ele));
  ele.cl_subject = clone.se_headline_issue;
  delete ele.se_headline_issue;
  ele.cl_contact_method = clone.se_contact_method;
  delete ele.se_contact_method;
  ele.cl_summary = clone.se_contact_description;
  delete ele.se_contact_description;
  ele.cl_date = clone.se_contact_date_timestamp_date;
  delete ele.se_contact_date_timestamp_date;
  ele.cl_contact_time = clone.se_contact_date_timestamp_time;
  delete ele.se_contact_date_timestamp_time;
  if(ele.cl_contact_method === 'In-Person') ele.cl_contact_method = 'In person';
  if(ele.cl_contact_method === 'Phone') ele.cl_contact_method = 'Telephone';
}

processFile();

module.exports = {
  Contact_log: {
    data: require('./new-contact-logs-modified.json')
  },
  Contract_area: {
    data: require('./contract-areas.json'),
  },
  Escalation_tier: {
    data: require('./escalation_tiers.json'),
  },
  Interest: {
    data: require('./interests.json'),
  },
  Licence: {
    data: [...require('./licences.json'), ...require('./new_licences_modified.json')],
  },
  Licence_group: {
    data: require('./licence-groups.json'),
  },
  Licence_status: {
    data: require('./licence_statuses.json'),
  },
  Organisation: {
    data: require('./organisations.json'),
  },
  Party_concern: {
    data: require('./party_concerns.json')
  },
  Person: {
    data: require('./people.json')
  },
  Suitability_code: {
    data: require('./suitability-codes.json'),
  },
  Surveyor: {
    data: require('./surveyors.json'),
  },
  Site: {
    data: require('./sites.json'),
  },
  Party: {
    data: require('./lrs-parties2.json')
  },
  Agent_org: {
    data: require('./agent-orgs.json'),
  },
};
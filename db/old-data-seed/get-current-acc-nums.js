const {writeFile} = require('fs');
const {promisify} = require('util');
const writeFileAsync = promisify(writeFile);

function processFile () {
  let arr = require('./licences.json');
  const accNums = arr.map(updateFunc);
  return writeFileAsync('db/old-data-seed/acc_nums.json', JSON.stringify(accNums, null, 2))
    .catch(console.log);
}

function updateFunc (ele) {
  return ele.l_acc_number;
}

processFile();
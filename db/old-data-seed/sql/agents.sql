SELECT 
    [lt_agent_organisation]
    ,[lt_agent_name]
    ,[lt_agent_address]
    ,[lt_agent_contact_number]
    ,[lt_agent_email]
FROM [HS2_Phase_2B].[dbo].[t_license_tracker]
WHERE (lt_agent_name != 'n/a' AND lt_agent_name != '') OR (lt_agent_organisation != 'n/a' AND lt_agent_organisation != '')
GROUP BY lt_agent_name, lt_agent_organisation, lt_agent_address, lt_agent_contact_number, lt_agent_email
ORDER BY lt_agent_organisation, lt_agent_name
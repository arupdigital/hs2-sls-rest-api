const { writeFileSync } = require('fs');

function processFile () {
  try {
    const {features} = require('./new-interests-geo.json');
    const {arangoInterests, pgInterests} = processInterests(features);
    writeFileSync(
      'db/old-data-seed/new-interests-arango-modified.json',
      JSON.stringify(arangoInterests, null, 2)
    );
    writeFileSync(
      'db/old-data-seed/new-interests-geo-modified.json',
      JSON.stringify(pgInterests, null, 2)
    );
  } catch (err) { console.log(err); }
}

const processInterests = features => {
  let arangoInterests = [];
  let pgInterests = [];

  features.forEach(feature => {

    pgInterests.push(feature);

    const {
      OBJECTID: i_object_id,
      LicenceNum,
      FullPartialTitle,
      Multipart,
      TitleNum: i_title_id
    } = feature.properties;

    const i_multipart = Multipart === 'Y';
    const i_full_title = FullPartialTitle === 'Full';

    arangoInterests.push({
      LicenceNum,
      i_full_title,
      i_object_id,
      i_multipart,
      i_title_id
    });
  });

  return {
    arangoInterests,
    pgInterests,
  };
};

processFile();

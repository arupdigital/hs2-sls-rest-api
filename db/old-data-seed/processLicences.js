const { writeFileSync } = require('fs');

function processFile () {
  try {
    const licences = require('./new_licences.json');
    const noiseLicences = require('./new_noise_licences.json');
    licences.forEach(updateFunc);
    noiseLicences.forEach(licence => {
      updateFunc(licence);
      updateNoiseLicence(licence);
    });
    return writeFileSync('db/old-data-seed/new_licences_modified.json', JSON.stringify([...licences, noiseLicences], null, 2));
  } catch (err) { console.log(err); }
}

function updateFunc (ele) {
  delete ele.id;
  Object.keys(ele).forEach(key => {
    if(ele[key]) ele[key] = ele[key].trim();
    if(ele[key] === '' || ele[key] === 'n/a') delete ele[key];
    if(key.includes('date') && ele[key]) {
      const [day, month, year] = ele[key].split('/');
      ele[key] = `${year}-${month}-${day}`;
    }
  });
  if(ele.l_site_visits) ele.l_site_visits = +ele.l_site_visits;
}

function updateNoiseLicence (ele) {
  ele.l_acc_number = ele.l_acc_number + '-N';
  ele.l_cro_number = ele.l_cro_number + '-N';
  ele.l_pln_number = ele.l_pln_number + '-N';
  ele.l_is_noise = true;
}

processFile();
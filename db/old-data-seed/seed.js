const { connectToDB, getArangoCol, connectToGeoDB } = require('../db');
const data = require('./');
const colModels = require('../../models/collection');
const omit = require('lodash.omit');
const {deepClone} = require('../../utils');
const {edgeConfig, edgeRenameRef} = require('../../controllers/arango/edgeConfig');
const {insertNulls} = require('../../utils/controller');
const nodeConfig = require('../data/dev/nodes');
const Joi = require('joi');
const difference = require('lodash.difference');
const edgeSpec = require('./edge-spec');
const graphs = require('../data/dev/nodes/graphs.json');
const edges = require('../data/dev/nodes/edges.json');
const edgeNames = Object.keys(edges);
const geoData = require('./geo-data');
const seedGeoDB = require('../seed/pg-seed');

function moveToDataDump (data, edgeSpec) {
  Object.entries(data).forEach(([colName, docs]) => {
    const edges = edgeConfig[colName] ? edgeConfig[colName].reduce((acc, edgeName) => {
      acc[edgeRenameRef[edgeName]] = null;
      return acc;
    }, {}) : null;
    docs.data.forEach(doc => {
      let _del = false;
      if(doc.hasOwnProperty('_del')) {
        _del = Boolean(doc._del);
        delete doc._del;
      }
      const {schema} = colModels[colName];
      const {error} = schema.validate(doc, {abortEarly: false});
      if(error) {
        doc.data_dump = {};
        doc.data_dump_complete = {};
        const clone = deepClone(doc);
        error.details.forEach(({path}) => {
          const field = path[0];
          if(
            field !== undefined && field !== 'undefined' &&
            !edgeSpec.find(ele => (
              (ele.fromVertColName === colName && ele.fromKey === field) ||
              (ele.toVertColName === colName && ele.toKey === field)
            ))
          ) {
            if(doc[field] !== null) {
              doc.data_dump[field] = clone[field];
              doc.data_dump_complete[field] = false;
            }
            delete doc[field];
          }
        });
        if(Object.keys(doc.data_dump).length === 0) {
          delete doc.data_dump;
          delete doc.data_dump_complete;
        }
        addMissingFields(doc, schema, edgeSpec, colName);
      }
      doc._del = _del;
      insertNulls(doc, schema, edges);
    });
  });
}

const dropGraphs = async (db) => {
  let graphs = await db.listGraphs();
  graphs = graphs.map(({ _key: name }) => db.graph(name));
  const graphPromises = graphs.map(graph => graph.drop());
  return Promise.all(graphPromises);
};


const createGraphs = async (db, graphInfo, edgeInfo) => {
  const graphs = Object.values(graphInfo).map((graph) => db.graph(graph.name));
  
  const graphPromises = graphs.map(graph => {
    const graphRef = graphInfo[graph['name']];
    const edgeDefinitions = graphRef.edgeDefinitions.map(edgeName => {
      const { name: collection, from, to } = edgeInfo[edgeName];
      return {
        collection,
        from: from,
        to: to
      };
    });
    return graph.create({ edgeDefinitions });
  });

  return Promise.all(graphPromises);
};

function addMissingFields (doc, schema, edgeSpec, colName) {
  const schemaDesc = Joi.describe(schema).children;
  const docKeys = Object.keys(doc);
  const schemaKeys = Object.keys(schemaDesc).filter(field => {
    let edgeColName = Object.entries(edgeRenameRef).find(([key, val]) => {
      key;
      return val === field;
    }) || [];
    const relevantEdgeSpecs = edgeSpec.filter(ele => {
      return ele.newEdgeColName === edgeColName[0] && ele.fromVertColName === colName;
    });
    if(relevantEdgeSpecs && relevantEdgeSpecs.length > 0) {
      const edgePresent = relevantEdgeSpecs.some(({fromKey, toVertColName, toKey}) => {
        return doc[fromKey] && data[toVertColName] && data[toVertColName].data.some(ele => ele[toKey] === doc[fromKey]);
      });
      if(edgePresent) docKeys.push(field);
    }
    const conditionallyRequired = (
      schemaDesc[field].type === 'alternatives' &&
      schemaDesc[field].alternatives &&
      doc[schemaDesc[field].alternatives[0].ref.slice(4)] === true
    );

    return (
      !schemaDesc[field].flags ||
      (schemaDesc[field].flags.presence !== 'optional' && schemaDesc[field].type !== 'alternatives') ||
      conditionallyRequired
    );
  });
  doc.missing_fields = difference(schemaKeys, docKeys);
}

function seedDocs(data) {
  return Promise.all(Object.entries(data).map(([colName, docs]) => {
    const col = getArangoCol(colName);
    return col.truncate()
      .then(() => Promise.all(docs.data.map(doc => {
        return col.save(doc)
          .catch(({message}) => console.error({message, doc}));
      })));
  }));
}

function linkEdgesSequentially (db, edgeSpec) {
  return edgeSpec.reduce((acc, cur) => {
    return acc.then(acc => linkEdges(db, cur).then(() => acc));
  }, Promise.resolve());
}

function linkEdges (db, edgeSpec) {
  const {newEdgeColName, fromVertColName, fromKey, keepFromKey, toVertColName, toKey, keepToKey} = edgeSpec;
  const fromCol = getArangoCol(fromVertColName);
  const toCol = getArangoCol(toVertColName);
  const newEdgeCol = db.edgeCollection(newEdgeColName);

  return newEdgeCol.exists()
    .then(exists => {
      return Promise.all([
        fromCol.all({batchSize: 999999}),
        toCol.all({batchSize: 999999}),
        exists ? null : newEdgeCol.create()
      ]);
    })
    .then(([fromDocs, toDocs]) => {
      fromDocs = fromDocs._result;
      toDocs = toDocs._result;
      return Promise.all(fromDocs.map(fromDoc => {
        const toMatches = toDocs.filter(toDoc => toDoc[toKey] === fromDoc[fromKey]);
        return Promise.all(toMatches.map(toMatch => {
          return newEdgeCol.save({_from: fromDoc._id, _to: toMatch._id})
            .then(() => {
              return Promise.all([
                keepFromKey ? null : fromCol.replace(fromDoc._id, omit(fromDoc, [fromKey, '_rev'])),
                keepToKey ? null : toCol.replace(toMatch._id, omit(toMatch, [toKey, '_rev']))
              ]);
            });
        }));
      }));
    });
}

const dropCollections = async (db) => {
  const collections = await db.collections();
  const collectionPromises = collections.map(col => col.drop());
  return Promise.all(collectionPromises);
};

const createEdgeCollections = async (db, edgeNames) => {
  const edgeColPromises = edgeNames.map(async edge => {
    edge = db.edgeCollection(edge);
    await edge.create({keyOptions:{type:'autoincrement'}});
    return edge;
  });
  return Promise.all(edgeColPromises);
};

const createNodeCollections = async (db, nodeConfig) => {
  const nodeColPromises = Object.keys(nodeConfig).map(async colName => {
    const col = db.collection(colName);
    await col.create({keyOptions:{type:'autoincrement'}});
    if(nodeConfig[colName].uniqueFields) {
      await Promise.all(nodeConfig[colName].uniqueFields.map((({fields, sparse}) => {
        return col.createHashIndex(fields, {unique: true, sparse});
      })));
    }
    return col;
  });
  return Promise.all(nodeColPromises);
};

(() => {
  let db;
  let geoDb;
  return Promise.all([connectToDB(), connectToGeoDB()])
    .then(res => {
      db = res[0];
      geoDb = res[1];
      return Promise.all([
        dropCollections(db)
          .then(() => createNodeCollections(db, nodeConfig))
          .then(() => createEdgeCollections(db, edgeNames))
          .then(() => {
            moveToDataDump(data, edgeSpec);
            return seedDocs((data));
          })
          .then(() => linkEdgesSequentially(db, edgeSpec))
          .then(() => dropGraphs(db))
          .then(() => createGraphs(db, graphs, edges))
          .catch(([message]) => console.error(message)),
        seedGeoDB(geoDb, geoData)
      ])
        .then(() => {
          console.log('Seeding complete');
          db.close();
          geoDb.close();
        });
    });
})();
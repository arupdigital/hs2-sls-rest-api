const {deepClone} = require('../../utils');
const countries = require('./countries.json');

function processParties (ele, i) {

  ele.p_id = i+1;

  Object.keys(ele).forEach(key => {
    if(key.includes('Address') && ele[key !== null]) ele[key] = String(ele[key]);
    if(ele[key] && typeof ele[key] === 'string') ele[key] = ele[key].trim();
  });

  const countryMatch = countries.find(obj => obj.name.includes(ele.internationalCountry));
  if(countryMatch) ele.internationalCountry = countryMatch['alpha-2'];
  
  const clone = deepClone(ele);

  
  ele.partyRef = clone.oldPartyRef;
  delete ele.oldPartyRef;

  
  extractCorrespondence(ele);
  extractCities(ele);
  extractCounties(ele);
  removeDupeName(ele);
}

function removeDupeName (ele) {
  const clone = deepClone(ele);
  if(ele.partyName === ele.propertyOwnerAddress1) {
    for (let lineNum = 1; lineNum < 7; lineNum ++) {
      ele[`propertyOwnerAddress${lineNum}`] = clone[`propertyOwnerAddress${lineNum + 1}`];
    }
    ele.propertyOwnerAddress7 = null;
  }
}

function extractCorrespondence (ele) {
  const clone = deepClone(ele);
  if(['The Secretary', 'The Chief Executive'].includes(ele.propertyOwnerAddress1)) {
    ele.correspondence_name = clone.propertyOwnerAddress1;
    for (let lineNum = 1; lineNum < 7; lineNum ++) {
      ele[`propertyOwnerAddress${lineNum}`] = clone[`propertyOwnerAddress${lineNum + 1}`];
    }
    ele.propertyOwnerAddress7 = null;
  }
}

function extractCounties (ele) {
  const clone = deepClone(ele);
  const addressKeys = Object.keys(ele).filter(key => /^.*address.*$/i.test(key));
  addressKeys.forEach(key => {
    if(
      /^(Bedfordshire|Buckinghamshire|Cambridgeshire|Cheshire|Cleveland|Cornwall|Cumbria|Derbyshire|Devon|Dorset|Durham|East Sussex|Essex|Gloucestershire|Greater London|Greater Manchester|Hampshire|Hertfordshire|Kent|Lancashire|Leicestershire|Lincolnshire|Merseyside|Norfolk|North Yorkshire|Northamptonshire|Northumberland|Nottinghamshire|Oxfordshire|Shropshire|Somerset|South Yorkshire|Staffordshire|Suffolk|Surrey|Tyne and Wear|Warwickshire|West Berkshire|West Midlands|West Sussex|West Yorkshire|Wiltshire|Worcestershire|Flintshire|Glamorgan|Merionethshire|Monmouthshire|Montgomeryshire|Pembrokeshire|Radnorshire|Anglesey|Breconshire|Caernarvonshire|Cardiganshire|Carmarthenshire|Denbighshire|Aberdeen City|Aberdeenshire|Angus|Argyll and Bute|City of Edinburgh|Clackmannanshire|Dumfries and Galloway|Dundee City|East Ayrshire|East Dunbartonshire|East Lothian|East Renfrewshire|Eilean Siar|Falkirk|Fife|Glasgow City|Highland|Inverclyde|Midlothian|Moray|North Ayrshire|North Lanarkshire|Orkney Islands|Perth and Kinross|Renfrewshire|Scottish Borders|Shetland Islands|South Ayrshire|South Lanarkshire|Stirling|West Dunbartonshire|West Lothian|Antrim|Armagh|Down|Fermanagh|Derry and Londonderry|Tyrone)$/i
        .test(ele[key])
    ) {
      let lineNum = Number(key.match(/\d/)[0]);
      ele.p_county = clone[key];
      for (lineNum; lineNum < 7; lineNum ++) {
        ele[`propertyOwnerAddress${lineNum}`] = clone[`propertyOwnerAddress${lineNum + 1}`];
      }
      ele.propertyOwnerAddress7 = null;
    }
  });
}

function extractCities (ele) {
  const clone = deepClone(ele);
  const addressKeys = Object.keys(ele).filter(key => /^.*address.*$/i.test(key));
  addressKeys.forEach(key => {
    if(
      /^(Barnsley|Basildon|Basingstoke|Blackpool|Bath|Bedford|Birkenhead|Bolton|Bournemouth|Birmingham|Blackburn|Bracknell|Bradford|Brighton and Hove|Bury|Bristol|Burnley|Burton upon Trent|Cambridge|Cardiff|Gateshead|Carlisle|Chatham|Chelmsford|Cheltenham|Chester|Chesterfield|Colchester|Derby|Coventry|Crawley|Darlington|Doncaster|Dudley|Eastbourne|Exeter|Gillingham|Gloucester|Grimsby|Guildford|Halifax|Harlow|Harrogate|Hartlepool|Hastings|Hemel Hempstead|High Wycombe|Huddersfield|Ipswich|South Shields|Kingston upon Hull|Leeds|Middlesbrough|Leicester|Lincoln|Manchester|Liverpool|London|Luton|Maidstone|Mansfield|Milton Keynes|Newcastle upon Tyne|Newcastle-under-Lyme|Newport|Northampton|Norwich|Nottingham|Nuneaton|Oldham|Oxford|Peterborough|Preston|Plymouth|Poole|Portsmouth|Reading|Redditch|Rochdale|Sutton Coldfield|Rotherham|Salford|Scunthorpe|Slough|Sheffield|Shrewsbury|Solihull|Southampton|Southend-on-Sea|Southport|St Albans|Wigan|St Helens|Stevenage|Stockport|Stockton-on-Tees|Woking|Stoke-on-Trent|Sunderland|Swansea|Swindon|Telford|Wakefield|Walsall|Warrington|Watford|West Bromwich|Weston-Super-Mare|Wolverhampton|Worcester|Worthing|York)$/i
        .test(ele[key])
    ) {
      let lineNum = Number(key.match(/\d/)[0]);
      ele.p_city = clone[key];
      for (lineNum; lineNum < 7; lineNum ++) {
        ele[`propertyOwnerAddress${lineNum}`] = clone[`propertyOwnerAddress${lineNum + 1}`];
      }
      ele.propertyOwnerAddress7 = null;
    }
  });
}

module.exports = processParties;
const {writeFile} = require('fs');
const {promisify} = require('util');
const writeFileAsync = promisify(writeFile);
const agents = require('./agentsv2.json');

function fixAgents (agents) {
  // const result = [];
  // agents.forEach(agentDoc => {
  //   const matches = agents.filter(agentDoc2 => {
  //     return (agentDoc2.lt_agent_organisation === agentDoc.lt_agent_organisation &&
  //     agentDoc2.lt_agent_name === agentDoc.lt_agent_name);
  //   });
  //   if(matches.length === 1) result.push(agentDoc);
  //   else if (!result.some(resDoc => {
  //     return (resDoc.lt_agent_organisation === agentDoc.lt_agent_organisation &&
  //     resDoc.lt_agent_name === agentDoc.lt_agent_name);
  //   })) {
  //     const mergedDoc = {};
  //     Object.keys(agentDoc).forEach(key => {
  //       let longestVal = matches.reduce((acc, match) => {
  //         if(match[key] && match[key].length > acc.length) acc = match[key];
  //         return acc;
  //       }, '');
  //       if(longestVal === '') longestVal = null;
  //       mergedDoc[key] = longestVal;
  //     });
  //     result.push(mergedDoc);
  //   }
  // });

  agents.forEach(agent => {
    let {lt_agent_name, lt_agent_address, lt_agent_organisation} = agent;
    const addReg = new RegExp(`${lt_agent_name ? lt_agent_name + '|' : ''}c/o${lt_agent_organisation ? '|' + lt_agent_organisation : ''}}`, 'ig');
    if(lt_agent_address && lt_agent_address.search(addReg)) lt_agent_address = lt_agent_address.replace(addReg, '');
  });

  return writeFileAsync('db/old-data-seed/agentsv3.json', JSON.stringify(agents, null, 2))
    .catch(console.log);
}

fixAgents(agents);
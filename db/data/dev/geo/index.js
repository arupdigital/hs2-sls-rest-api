const uniqueInterests = require('./uniqueInterests.json');
const largeTitles = require('./largeTitles.json');

module.exports = [
  {
    name: 'interests',
    prefix: 'i',
    data: uniqueInterests.concat(largeTitles)
  }
  // {
  //   name: 'boundaries',
  //   prefix: 'b',
  //   data: require('./boundaries.json')
  // }
];
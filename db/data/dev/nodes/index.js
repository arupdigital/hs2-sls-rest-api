module.exports = {
  Contact_log: {
    data: require('./contact-logs.json')
  },
  Contract_area: {
    data: require('./contract-areas.json'),
    uniqueFields: [{fields:'c_area'}]
  },
  Escalation_tier: {
    data: require('./escalation-tiers.json'),
    uniqueFields: [{fields:'et_tier'}]
  },
  Hazard: {
    data: require('./hazards.json'),
    uniqueFields: [{fields:'h_hazard'}]
  },
  Interest: {
    data: require('./interests.json'),
    uniqueFields: [
      {fields:'i_object_id'}]
    // {fields:'i_title_id'}]
  },
  Licence: {
    data: require('./licences.json'),
    uniqueFields: [
      {fields:'l_acc_number'},
      {fields:'l_cro_number'},
      {fields:'l_pln_number'}
    ]
  },
  Licence_group: {
    data: require('./licence-groups.json'),
    uniqueFields: [{fields:'lg_name'}]
  },
  Licence_status: {
    data: require('./licence-status.json'),
    uniqueFields: [{fields:'ls_status'}]
  },
  Organisation: {
    data: require('./organisations.json'),
  },
  Party_concern: {
    data: require('./party-concerns.json')
  },
  Person: {
    data: require('./people.json')
  },
  Suitability_code: {
    data: require('./suitability-codes.json'),
    uniqueFields: [{fields:'sc_code'}]
  },
  Surveyor: {
    data: require('./surveyors.json'),
    uniqueFields: [{fields:'sv_name'}]
  },
  Survey_type: {
    data: require('./survey-types.json'),
    uniqueFields: [{fields:'st_id'}]
  },
  Site: {
    data: require('./sites.json'),
    uniqueFields: [{fields:'s_id'}]
  },
  Party: {
    data: require('./parties.json'),
    // uniqueFields: [{fields:'pa_ref'}]
  },
  Agent_org: {
    data: require('./agent-orgs.json'),
    uniqueFields: [{fields:'ao_name'}]
  },
  User: {
    data: require('./users.json'),
    uniqueFields: [{fields:'u_email'}]
  }
};
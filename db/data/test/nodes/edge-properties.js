
const getEdgeProps = (name) => {
  switch(name) {
  case 'has_contact' : 
    return [
      '_key',
      '_id',
      '_from',
      '_to',
      '_rev',
      'is_primary'
    ];
  case 'reported_by' : 
    return [
      '_key',
      '_id',
      '_from',
      '_to',
      '_rev',
      'date_reported'
    ];
  case 'actioned_by': 
    return [
      '_key',
      '_id',
      '_from',
      '_to',
      '_rev',
      'date_actioned'
    ];
  default : 
    return [
      '_key',
      '_id',
      '_from',
      '_to',
      '_rev'
    ];
  }

};

module.exports = getEdgeProps;

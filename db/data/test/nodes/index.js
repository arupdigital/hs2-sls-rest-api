module.exports = {
  Contact_log: {
    data: require('./contact-logs.json')
  },
  Contract_area: {
    data: require('./contract-areas.json'),
    uniqueFields: ['c_area']
  },
  Escalation_tier: {
    data: require('./escalation-tiers.json'),
    uniqueFields: ['et_tier']
  },
  Hazard: {
    data: require('./hazards.json'),
    uniqueFields: ['h_hazard']
  },
  Interest: {
    data: require('./interests.json'),
    uniqueFields: ['i_object_id']
  },
  Licence: {
    data: require('./licences.json'),
    uniqueFields: ['l_acc_number']
  },
  Licence_group: {
    data: require('./licence-groups.json'),
    uniqueFields: ['lg_name']
  },
  Licence_status: {
    data: require('./licence-status.json'),
    uniqueFields: ['ls_status']
  },
  Organisation: {
    data: require('./organisations.json'),
  },
  Party_concern: {
    data: require('./party-concerns.json')
  },
  Person: {
    data: require('./people.json')
  },
  Phase: {
    data: require('./phases.json'),
    uniqueFields: ['p_code']
  },
  Suitability_code: {
    data: require('./suitability-codes.json'),
    uniqueFields: ['sc_code']
  }
};
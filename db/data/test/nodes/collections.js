module.exports = {
  'reported_by': { 
    name: 'reported_by',
    type: 3,
    isSystem: false,
  },
  'has_interest':  {
    name: 'has_interest',
    type: 3,
    isSystem: false,
  },
  'has_licencee': { 
    name: 'has_licencee',
    type: 3,
    isSystem: false,
  },
  'has_agent': {
    name: 'has_agent',
    type: 3,
    isSystem: false,
  },
  'has_escalation_tier': { 
    name: 'has_escalation_tier',
    type: 3,
    isSystem: false,
  },
  'has_group': { 
    name: 'has_group',
    type: 3,
    isSystem: false,
  },
  'has_originator': { 
    name: 'has_originator',
    type: 3,
    isSystem: false,
  },
  'has_status': { 
    name: 'has_status',
    type: 3,
    isSystem: false,
  },
  'Suitability_code': { 
    name: 'Suitability_code',
    type: 2,
    isSystem: false,
  },
  'Person': { 
    name: 'Person',
    type: 2,
    isSystem: false,
  },
  'Organisation': { 
    name: 'Organisation',
    type: 2,
    isSystem: false,
  },
  'Licence_group': { 
    name: 'Licence_group',
    type: 2,
    isSystem: false,
  },
  'Licence_status': { 
    name: 'Licence_status',
    type: 2,
    isSystem: false,
  },
  'Interest': { 
    name: 'Interest',
    type: 2,
    isSystem: false,
  },
  'Escalation_tier': { 
    name: 'Escalation_tier',
    type: 2,
    isSystem: false,
  },
  'Contract_area': { 
    name: 'Contract_area',
    type: 2,
    isSystem: false,
  },
  'Contact_log': { 
    name: 'Contact_log',
    type: 2,
    isSystem: false,
  },
  'Party_concern': { 
    name: 'Party_concern',
    type: 2,
    isSystem: false,
  },
  'has_participant': { 
    name: 'has_participant',
    type: 3,
    isSystem: false,
  },
  'Licence': { 
    name: 'Licence',
    type: 2,
    isSystem: false,
  },
  'has_contact': { 
    name: 'has_contact',
    type: 3,
    isSystem: false,
  },
  'Phase': { 
    name: 'Phase',
    type: 2,
    isSystem: false,
  },
  'actioned_by': {
    name: 'actioned_by',
    type: 3,
    isSystem: false,
  },
  'Hazard': { 
    name: 'Hazard',
    type: 2,
    isSystem: false,
  },
  'relates_to': {
    name: 'relates_to',
    type: 3,
    isSystem: false
  } };

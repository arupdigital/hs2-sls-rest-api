const Joi = require('joi');
const {isoDate} = require('../collection/core');

const config = {
  _from: Joi.string().regex(/^Party_concern\/\d+$/),
  _to: Joi.string().regex(/^Person\/\d+$/).required(),
  date_actioned: isoDate.required()
};

const schema = Joi.object().keys(config);

module.exports = {schema, config};
const Joi = require('joi');

const config = {
  _from: Joi.string().regex(/^Party\/\d+$/),
  _to: Joi.string().regex(/^(Person|Organisation)\/\d+$/).required()
};

const schema = Joi.object().keys(config);

module.exports = {schema, config};
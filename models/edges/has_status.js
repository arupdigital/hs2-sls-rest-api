const Joi = require('joi');
const {isoDate} = require('../collection/core');

const config = {
  _from: Joi.string().regex(/^Licence\/\d+$/),
  _to: Joi.string().regex(/^Licence_status\/\d+$/).required(),
  date_changed: isoDate.required(),
  reason_for_refusal: Joi.string().min(4).max(500).optional(),
  is_current: Joi.bool().required()
};

const schema = Joi.object().keys(config);

module.exports = {schema, config};
const Joi = require('joi');
const {isoDate} = require('../collection/core');

const config = {
  _from: Joi.string().regex(/^Licence\/\d+$/),
  _to: Joi.string().regex(/^Escalation_tier\/\d+$/).required(),
  date_changed: isoDate.required(),
  reason_for_change: Joi.string().max(500).optional(),
  is_current: Joi.boolean().required()
};

const schema = Joi.object().keys(config);

module.exports = {schema, config};
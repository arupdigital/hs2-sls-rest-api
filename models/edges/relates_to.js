const Joi = require('joi');

const config = {
  _from: Joi.string().regex(/^Contact_log\/\d+$/),
  _to: Joi.string().regex(/^Licence\/\d+$/).required(),
};

const schema = Joi.object().keys(config);

module.exports = {schema, config};
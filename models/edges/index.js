const { getArangoCol } = require('../../db/db');

const arangoEdgeCollections = [
  {
    name: 'has_status',
    ...require('./has_status')
  },
  {
    name: 'has_group',
    ...require('./has_group')
  },
  {
    name: 'has_escalation_tier',
    ...require('./has_escalation_tier')
  },
  {
    name: 'has_agent',
    ...require('./has_agent')
  },
  {
    name: 'has_licencee',
    ...require('./has_licencee')
  },
  {
    name: 'has_contact',
    ...require('./has_contact')
  },
  {
    name: 'has_interest',
    ...require('./has_interest')
  },
  {
    name: 'made_contact',
    ...require('./made_contact')
  },
  {
    name: 'received_contact',
    ...require('./received_contact')
  },
  {
    name: 'reported_by',
    ...require('./reported_by')
  },
  {
    name: 'actioned_by',
    ...require('./actioned_by')
  },
  {
    name: 'relates_to',
    ...require('./relates_to')
  },
  {
    name: 'collected_by',
    ...require('./collected_by')
  },
  {
    name: 'has_survey_type',
    ...require('./has_survey_type')
  },
  {
    name: 'contains_interest',
    ...require('./contains_interest')
  },
  {
    name: 'has_contract_area',
    ...require('./has_contract_area')
  },
  {
    name: 'has_member',
    ...require('./has_member')
  },
  {
    name: 'has_agent_org',
    ...require('./has_agent_org')
  },
  // {
  //   name: 'has_concern',
  //   ...require('./has_concern')
  // },
  {
    name: 'next_action_by',
    ...require('./next_action_by')
  }
  ,
  {
    name: 'related_licence',
    ...require('./related_licence')
  }
];

const arangoEdgeModels = arangoEdgeCollections.reduce((acc, col) => {
  acc[col.name] = getArangoCol(col.name);
  acc[col.name].schema = col.schema;
  acc[col.name].config = col.config;
  return acc;
}, {});

module.exports = arangoEdgeModels;
const Joi = require('joi');

const config = {
  _from: Joi.string().regex(/^Contact_log\/\d+$/),
  _to: Joi.string().regex(/^Person\/\d+$/).required(),
  is_contact_log_agent: Joi.bool().required()
};

const schema = Joi.object().keys(config);

module.exports = {schema, config};
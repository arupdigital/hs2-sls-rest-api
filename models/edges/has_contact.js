const Joi = require('joi');

const config = {
  _from: Joi.string().regex(/^Organisation\/\d+$/),
  _to: Joi.string().regex(/^Person\/\d+$/).required(),
  is_primary: Joi.boolean().required(),
  officer_title: Joi.valid([
    'The Chief Executive', 'The Clerk', 'The Secretary', 'For Access Licences'
  ]).optional()
};

const schema = Joi.object().keys(config);

module.exports = {schema, config};
const Joi = require('joi');

const config = {
  _from: Joi.string().regex(/^Site\/\d+$/),
  _to: Joi.string().regex(/^Interest\/\d+$/).required()
};

const schema = Joi.object().keys(config);

module.exports = {schema, config};
const Joi = require('joi');

const config = {
  _from: Joi.string().regex(/^(Party|Licence)\/\d+$/),
  _to: Joi.string().regex(/^Interest\/\d+$/).required(),
  tenure: Joi.valid('FH', 'TT', 'LL', 'NLR', 'O/O').optional(),
  // is_occupier: Joi.bool().optional()
};

const schema = Joi.object().keys(config);

module.exports = {schema, config};
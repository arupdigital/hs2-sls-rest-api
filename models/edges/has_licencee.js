const Joi = require('joi');

const config = {
  _from: Joi.string().regex(/^Licence\/\d+$/),
  _to: Joi.string().regex(/^(Party)\/\d+$/).required()
};

const schema = Joi.object().keys(config);

module.exports = {schema, config};
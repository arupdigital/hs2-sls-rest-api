const Joi = require('joi');

const config = {
  _from: Joi.string().regex(/^Person\/\d+$/),
  _to: Joi.string().regex(/^Agent_org\/\d+$/).required()
};

const schema = Joi.object().keys(config);

module.exports = {schema, config};
const Joi = require('joi');

const config = {
  _from: Joi.string().regex(/^Interest\/\d+$/),
  _to: Joi.string().regex(/^Person\/\d+$/).required()
};

const schema = Joi.object().keys(config);

module.exports = {schema, config};
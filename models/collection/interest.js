const Joi = require('joi');
const {procEdgeSchema} = require('./core');
const {has_agent} = require('../edges');

const {
  address : {
    building_name,
    building_number,
    address_line,
    city,
    county,
    country,
    postcode
  }
} = require('./core');

const schema = Joi.object().keys({
  i_object_id: Joi.number().integer().min(0).max(999999),
  i_title_id: Joi.string().min(5).max(10).uppercase().alphanum().strict().regex(/([A-Z]{1,2}[0-9]{1,8})/),
  i_full_title: Joi.boolean(),
  i_multipart: Joi.boolean(),
  i_building_number: building_number.optional(),
  i_building_name: building_name.optional(),
  i_address_line_1: address_line.optional(),
  i_address_line_2: address_line.optional(),
  i_address_line_3: address_line.optional(),
  i_city: city.optional(),
  i_county: county.optional(),
  i_country: country.optional(),
  i_postcode: Joi.any().when('i_country', {
    is: 'GB',
    then: postcode.optional(),
    otherwise: Joi.string().max(10).optional()
  }),
  i_address_comments: Joi.string().max(500).optional(),
  i_fully_complete: Joi.bool(),
  i_comments: Joi.string().max(500).optional(),
  geoData: Joi.object().optional(),
  agent: procEdgeSchema(has_agent).optional(),
  data_dump: Joi.object().optional(),
  data_dump_complete: Joi.object().optional(),
})
  .with('i_address_line_2', 'i_address_line_1')
  .with('i_address_line_3', 'i_address_line_2');

module.exports = schema;
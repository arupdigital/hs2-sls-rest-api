const Joi = require('joi');
const {procEdgeSchema} = require('./core');
const {has_survey_type, has_contract_area, contains_interest} = require('../edges');

const schema = Joi.object().keys({
  s_id: Joi.when('s_is_cdes', {
    is: true,
    then: Joi.string().regex(/[A-Z]{1,2}\d{4,10}/),
    otherwise: Joi.string().regex(/(EES_L)(1|2|3)_(Wood|Pond|Land)\d{1,5}/)
  }),
  s_is_cdes: Joi.bool(),
  s_access_requirements: Joi.string().max(1000).optional(),
  s_access_date_time: Joi.string().max(100).optional(),
  s_hse_factors: Joi.string().max(500).optional(),
  s_land_use: Joi.string().max(500).optional(),
  s_asbestos: Joi.boolean().optional(),
  s_asbestos_details: Joi.when('s_asbestos', {
    is: true,
    then: Joi.string().max(500).optional(),
    otherwise: Joi.forbidden()
  }),
  s_crop_type: Joi.string().max(500).optional(),
  s_crop_height: Joi.number().positive().optional(),
  s_confined_restricted_areas: Joi.boolean().optional(),
  s_confined_restricted_details: Joi.when('s_confined_restricted_areas', {
    is: true,
    then: Joi.string().max(500).optional(),
    otherwise: Joi.forbidden()
  }),
  s_watercourse_on_site: Joi.boolean().optional(),
  s_watercourse_details: Joi.when('s_watercourse_on_site', {
    is: true,
    then: Joi.string().max(500).optional(),
    otherwise: Joi.forbidden()
  }),
  s_within_public_highway: Joi.boolean().optional(),
  s_within_public_highway_details: Joi.when('s_within_public_highway', {
    is: true,
    then: Joi.string().max(500).optional(),
    otherwise: Joi.forbidden()
  }),
  s_railway_on_site: Joi.boolean().optional(),
  s_railway_details: Joi.when('s_railway_on_site', {
    is: true,
    then: Joi.string().max(500).optional(),
    otherwise: Joi.forbidden()
  }),
  s_other_activities: Joi.string().max(500).optional(),
  s_unbranded_high_vis_required: Joi.boolean().optional(),
  s_unbranded_high_vis_details: Joi.when('s_unbranded_high_vis_required', {
    is: true,
    then: Joi.string().max(500).optional(),
    otherwise: Joi.forbidden()
  }),
  s_phone_signal_status: Joi.valid('High', 'Medium', 'Low').optional(),
  s_is_measham: Joi.bool().optional(),
  s_priority_level: Joi.valid('High', 'Medium', 'Low').optional(),
  survey_types: Joi.array().items(procEdgeSchema(has_survey_type)).min(1),
  contract_areas: Joi.array().items(procEdgeSchema(has_contract_area)).min(1),
  interests: Joi.array().items(procEdgeSchema(contains_interest)).min(1),
  data_dump: Joi.object().optional(),
  data_dump_complete: Joi.object().optional(),
});

module.exports = schema;
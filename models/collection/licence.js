const Joi = require('joi');
const {procEdgeSchema, isoDate} = require('./core');
const {collected_by, has_licencee, has_escalation_tier, has_status, has_group, has_interest, next_action_by} = require('../edges');

const revisionSchema = {
  l_revision_name: Joi.string().length(3).regex(/^P\d{2}$/),
  l_reason_for_revision: Joi.string().min(4).max(500).optional(),
  l_date_posted: isoDate.optional(),
  l_date_signed_by_lo: isoDate.optional(),
  l_approved_countersign_date: isoDate.optional(),
  l_date_countersigned: isoDate.optional(),
  l_payment_schedule_approval_date: isoDate.optional(),
  l_expiry_date: isoDate.optional(),
};

const schema = Joi.object().keys({
  l_acc_number: Joi.string().min(26).max(50).uppercase().strict().regex(/^(2C865-ARP-LP-ACC-100|C273-MMD-LP-ACC-030)-\d{6,}$/),
  l_cro_number: Joi.string().min(26).max(50).uppercase().strict().regex(/^2C865-(ARP|MMD)-LP-(ACC|CRO)-100-\d{6,}$/),
  l_pln_number: Joi.string().min(26).max(50).uppercase().strict().regex(/^2C865-(ARP|MMD)-LP-(ACC|PLN|LRS|CRO)-100-\d{6,}$/),
  l_revision_name: revisionSchema.l_revision_name,
  l_reason_for_revision: revisionSchema.l_reason_for_revision,
  l_date_posted: revisionSchema.l_date_posted,
  l_date_signed_by_lo: revisionSchema.l_date_signed_by_lo,
  l_approved_countersign_date: revisionSchema.l_approved_countersign_date,
  l_date_countersigned: revisionSchema.l_date_countersigned,
  l_payment_schedule_approval_date: revisionSchema.l_payment_schedule_approval_date,
  l_expiry_date: revisionSchema.l_expiry_date,
  l_is_noise: Joi.bool(),
  l_previous_revisions: Joi.array().items(Joi.object().keys(revisionSchema)).optional(),
  l_issue_status: Joi.string().max(500).optional(),
  l_next_action: Joi.string().max(500).optional(),
  l_next_action_due: isoDate.optional(),
  l_arup_general_comment: Joi.string().max(500).optional(),
  l_site_visits: Joi.number().integer().min(0).max(999),
  next_action_by: procEdgeSchema(next_action_by).optional(),
  l_next_action_by_other: Joi.string().max(50).optional(),
  licencee: procEdgeSchema(has_licencee),
  escalation_tier: Joi.array().items(procEdgeSchema(has_escalation_tier)).min(1),
  status: Joi.array().items(procEdgeSchema(has_status)).min(1),
  group: Joi.array().items(procEdgeSchema(has_group)).min(1),
  surveyor: procEdgeSchema(collected_by),
  interests: Joi.array().items(procEdgeSchema(has_interest)).min(1),
  data_dump: Joi.object().optional(),
  data_dump_complete: Joi.object().optional(),
})
  .with('l_reason_for_revision', 'l_revision_name');

schema.custom = {
  escalation_tier: data => {
    const result = {
      valid: true,
      message: null
    };
    const currents = data.escalation_tier.filter(ele => ele.is_current);
    if(currents.length !== 1) {
      result.valid = false;
      result.message = 'must have one current tier';
    }
    return result;
  },
  status: data => {
    const result = {
      valid: true,
      message: null
    };
    const currents = data.status.filter(ele => ele.is_current);
    if(currents.length !== 1) {
      result.valid = false;
      result.message = 'must have one current status';
    }
    return result;
  }
};

module.exports = schema;
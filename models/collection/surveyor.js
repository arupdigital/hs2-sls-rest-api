const Joi = require('joi');

const schema = Joi.object().keys({
  sv_name: Joi.string().uppercase().strict().max(3)
});

module.exports = schema;


const Joi = require('joi');

const schema = Joi.object().keys({
  h_hazard: Joi.string().max(50),
  h_buffer_size: Joi.number().integer().min(0).max(999999)
});

module.exports = schema;
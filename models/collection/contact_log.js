const Joi = require('joi');
const {relates_to, made_contact, received_contact} = require('../edges');
const {procEdgeSchema, isoDate, time} = require('./core');

const schema = Joi.object().keys({
  cl_subject: Joi.string().max(500).optional(),
  cl_summary: Joi.string().max(20000),
  cl_comments: Joi.string().max(500).optional(),
  cl_date: isoDate,
  cl_contact_time: time.optional(),
  cl_contact_method: Joi.valid('Telephone', 'Email', 'Letter', 'Fax', 'In person', 'Internal'),
  relates_to: procEdgeSchema(relates_to),
  from: Joi.array().items(procEdgeSchema(made_contact)).min(1),
  to: Joi.array().items(procEdgeSchema(received_contact)).min(1),
  data_dump: Joi.object().optional(),
  data_dump_complete: Joi.object().optional(),
});

// schema.custom = {
//   from: data => {
//     const result = {
//       valid: true,
//       message: null
//     };
//     const users = [...data.from.filter(ele => /(User)/.test(ele.id)), ...data.to.filter(ele => /(User)/.test(ele.id))];
//     if(users.length !== 1) {
//       result.valid = false;
//       result.message = 'must include one user';
//     }
//     return result;
//   }
// };

// Dummy data has not been changed to ensure at least one 'to' or 'from' is a User ID. Therefore the tests are failing.

module.exports = schema;
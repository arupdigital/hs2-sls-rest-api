const Joi = require('joi');

const schema = Joi.object().keys({
  lg_name: Joi.string().max(3).uppercase().strict()
});

module.exports = schema;
const Joi = require('joi');

const schema = Joi.object().keys({
  ao_name: Joi.string().max(50)
});

module.exports = schema;
const Joi = require('joi');
const {has_interest, has_member} = require('../edges');
const {procEdgeSchema, isoDate} = require('./core');

const schema = Joi.object().keys({
  pa_ref: Joi.string().min(7).max(10).uppercase().strict().regex(/^PM{0,1}\d{6}$/),
  pa_date_created: isoDate,
  pa_is_current: Joi.bool(),
  pa_is_mlo: Joi.bool(),
  pa_name: Joi.string().max(30).optional(),
  members: Joi.array().items(procEdgeSchema(has_member)).min(1),
  interests: Joi.array().items(procEdgeSchema(has_interest)).min(1),
  data_dump: Joi.object().optional(),
  data_dump_complete: Joi.object().optional(),
});

module.exports = schema;
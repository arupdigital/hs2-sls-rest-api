const Joi = require('joi');

const schema = Joi.object().keys({
  ls_status: Joi.string().max(30)
});

module.exports = schema;
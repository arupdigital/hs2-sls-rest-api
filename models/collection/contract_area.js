const Joi = require('joi');

const schema = Joi.object().keys({
  c_area: Joi.string().uppercase().max(3).alphanum().strict(),
});

module.exports = schema;
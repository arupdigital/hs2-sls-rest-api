const Joi = require('joi');
const {has_contact} = require('../edges');

const {
  procEdgeSchema,
  address: {
    building_name,
    building_number,
    address_line,
    city,
    county,
    country,
    postcode
  },
  telNum,
  email
} = require('./core');

const schema = Joi.object().keys({
  o_name: Joi.string().max(100),
  o_correspondence_name: Joi.string().max(100).optional(),
  o_email: email.optional(),
  o_tel_number: telNum.optional(),
  o_fax: telNum.optional(),
  o_building_name: building_name.optional(),
  o_building_number: building_number.optional(),
  o_address_line_1: address_line,
  o_address_line_2: address_line.optional(),
  o_address_line_3: address_line.optional(),
  o_city: city,
  o_county: county.optional(),
  o_country: country,
  o_postcode: Joi.any().when('o_country', {
    is: 'GB',
    then: postcode,
    otherwise: Joi.string().max(10).optional()
  }),
  o_company_number: Joi.string().max(20).optional(),
  o_circumstances: Joi.string().max(500).optional(),
  o_active: Joi.bool(),
  contacts: Joi.array().items(procEdgeSchema(has_contact)).optional(),
  data_dump: Joi.object().optional(),
  data_dump_complete: Joi.object().optional(),
})
  .or('o_building_name', 'o_building_number')
  .with('o_address_line_2', 'o_address_line_1')
  .with('o_address_line_3', 'o_address_line_2');

module.exports = schema;
const Joi = require('joi');
const {has_agent_org} = require('../edges');

const {
  procEdgeSchema,
  address: {
    building_name,
    building_number,
    apartment_number,
    address_line,
    city,
    county,
    country,
    postcode
  },
  telNum,
  email
} = require('./core');

const schema = Joi.object().keys({
  p_title: Joi.string().max(40),
  p_first_name: Joi.string().max(35),
  p_middle_names: Joi.string().max(100).optional(),
  p_last_name: Joi.string().max(35),
  p_email: email.optional(),
  p_home_tel: telNum.optional(),
  p_work_tel: telNum.optional(),
  p_mobile: telNum.optional(),
  p_fax: telNum.optional(),
  p_apartment_number: apartment_number.optional(),
  p_house_name: building_name.optional(),
  p_house_number: building_number.optional(),
  p_address_line_1: address_line,
  p_address_line_2: address_line.optional(),
  p_address_line_3: address_line.optional(),
  p_city: city,
  p_county: county.optional(),
  p_country: country,
  p_postcode: Joi.any().when('p_country', {
    is: 'GB',
    then: postcode,
    otherwise: Joi.string().max(10).optional()
  }),
  p_circumstances: Joi.string().max(500).optional(),
  p_salutation: Joi.string().min(5).max(100).optional(),
  p_deceased: Joi.bool(),
  agent_org: procEdgeSchema(has_agent_org).optional(),
  data_dump: Joi.object().optional(),
  data_dump_complete: Joi.object().optional(),
})
  .or('p_house_name', 'p_house_number')
  .with('p_address_line_2', 'p_address_line_1')
  .with('p_address_line_3', 'p_address_line_2');
  // .or('p_email', 'p_home_tel', 'p_work_tel', 'p_mobile', 'p_');

module.exports = schema;


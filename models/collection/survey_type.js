const Joi = require('joi');

const schema = Joi.object().keys({
  st_id: Joi.string().regex(/^[A-Z]+[0-9]+$/).max(9)
});

module.exports = schema;
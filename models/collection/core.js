let Joi = require('joi').extend(require('joi-phone-number'));
Joi = Joi.extend(require('joi-date-extensions'));
 
const address = {
  building_name: Joi.string().max(50),
  building_number: Joi.string().regex(/[0-9a-z]{1,6}/i),
  apartment_number: Joi.string().regex(/[0-9a-z]{1,4}/i),
  address_line: Joi.string().max(35),
  city: Joi.string().max(50),
  county: Joi.string().max(30),
  country: Joi.string().length(2).uppercase().regex(/^\w+$/).strict(),
  postcode: Joi.string().uppercase().strict().regex(/([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})/)
};

const telNum = Joi.string().phoneNumber();

const email = Joi.string().email().max(254);

const isoDate = Joi.date().iso().format('YYYY-MM-DD');

const time = Joi.date().format('HH:mm');

function procEdgeSchema (schema) {
  const newConfig = Object.keys(schema.config).reduce((acc, field) => {
    if(field !== '_to' && field !== '_from') acc[field] = schema.config[field];
    return acc;
  }, {});
  newConfig.id = schema.config._to;
  return Joi.object().keys(newConfig);
}

module.exports = {
  address,
  telNum,
  email,
  procEdgeSchema,
  isoDate,
  time
};
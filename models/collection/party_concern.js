const Joi = require('joi');
const { reported_by, actioned_by, related_licence } = require('../edges');
const {procEdgeSchema} = require('./core');

const schema = Joi.object().keys({
  pc_concern: Joi.string().max(5000),
  reported_by: procEdgeSchema(reported_by),
  actioned_by: procEdgeSchema(actioned_by).optional(),
  related_licence: procEdgeSchema(related_licence),
  data_dump: Joi.object().optional(),
  data_dump_complete: Joi.object().optional(),
});

module.exports = schema;
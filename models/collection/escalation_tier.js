const Joi = require('joi');

const schema = Joi.object().keys({
  et_tier: Joi.string().regex(/^Tier \d+$/).max(7)
});

module.exports = schema;
const { getArangoCol } = require('../../db/db');

const arangoCollections = [
  {
    name: 'Contact_log',
    schema: require('./contact_log')
  },
  {
    name: 'Contract_area',
    schema: require('./contract_area')
  },
  {
    name: 'Escalation_tier',
    schema: require('./escalation_tier')
  },
  {
    name: 'Hazard',
    schema: require('./hazard')
  },
  {
    name: 'Interest',
    schema: require('./interest')
  },
  {
    name: 'Licence_group',
    schema: require('./licence_group')
  },
  {
    name: 'Licence_status',
    schema: require('./licence_status')
  },
  {
    name: 'Licence',
    schema: require('./licence')
  },
  {
    name: 'Organisation',
    schema: require('./organisation')
  },
  {
    name: 'Person',
    schema: require('./person')
  },
  {
    name: 'Suitability_code',
    schema: require('./suitability_code')
  },
  {
    name: 'Party_concern',
    schema: require('./party_concern')
  },
  {
    name: 'Surveyor',
    schema: require('./surveyor')
  },
  {
    name: 'Survey_type',
    schema: require('./survey_type')
  },
  {
    name: 'Site',
    schema: require('./site')
  },
  {
    name: 'Party',
    schema: require('./party')
  },
  {
    name: 'Agent_org',
    schema: require('./agent_org')
  },
  {
    name: 'User',
    schema: require('./user')
  }
];

const arangoColModels = arangoCollections.reduce((acc, col) => {
  acc[col.name] = getArangoCol(col.name);
  acc[col.name].schema = col.schema;
  return acc;
}, {});

module.exports = arangoColModels;
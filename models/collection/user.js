const Joi = require('joi');

const schema = Joi.object().keys({
  u_email: Joi.string().email(),
  u_first_name: Joi.string().max(35),
  u_middle_names: Joi.string().max(50),
  u_last_name: Joi.string().max(35)
});

module.exports = schema;
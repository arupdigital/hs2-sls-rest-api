const Joi = require('joi');

const schema = Joi.object().keys({
  sc_name: Joi.string().max(30),
  sc_code: Joi.string().uppercase().alphanum().max(4).strict().regex(/^SC\d+$/)
});

module.exports = schema;


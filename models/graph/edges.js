const { getArangoDB } = require('../../db/db');
const graphs = require('./graphs.js');
const arangoEdgeModels = require('../edges');
const db = getArangoDB();

const getEdgeCollections = Object.keys(arangoEdgeModels);

const getGraphCollections = () => {
  return Object.keys(graphs).map(graphName => {
    const graph = db.graph(graphName);
    return {
      graph
    };
  });
};

const graphEdgeCollections = () => {
  const graphCollections = getGraphCollections();
  const graphEdgeCollections = graphCollections.reduce((acc, graphCol) => {
    const { graph } = graphCol;
    const edgeCollections = getEdgeCollections.reduce((acc, edge) => {
      acc[`${edge}`] = graph.edgeCollection(edge);
      return acc;
    }, {});
    acc[graph.name] = {
      graph,
      edgeCollections
    };
    return acc;
  }, {});
  return graphEdgeCollections;
};

const graphEdgeModels = () => {
  const graphEdgeCol = graphEdgeCollections();
  return Object.keys(graphEdgeCol).reduce((acc, graph) => {
    const { edgeCollections, graph: graphObj } = graphEdgeCol[graph];
    const edgeColName = Object.keys(edgeCollections);
    const edgeSchemas = edgeColName.reduce((acc, edgeName) => {
      const schema = arangoEdgeModels[edgeName].schema;
      const graphCol = edgeCollections[edgeName];
      acc[`${edgeName}`] = { name: edgeName, graphCol, schema, graphObj };
      return acc;
    }, {}); 
    acc[`${graph}`] = edgeSchemas;
    return acc;
  }, {});
};

module.exports = graphEdgeModels();

const db = require('../../db/db').getGeoDB();
const Sequelize = require('sequelize');

module.exports = db.define('t_hazards', {
  h_object_id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  h_geometry: {
    type: Sequelize.GEOMETRY,
    allowNull: false
  }
});
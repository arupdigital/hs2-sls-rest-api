const db = require('../../db/db').getGeoDB();
const Sequelize = require('sequelize');

module.exports = db.define('t_sites', {
  s_object_id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  s_geometry: {
    type: Sequelize.GEOMETRY,
    allowNull: false
  }
});
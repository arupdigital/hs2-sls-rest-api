const db = require('../../db/db').getGeoDB();
const Sequelize = require('sequelize');

module.exports = db.define('t_interests', {
  i_object_id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  i_geometry: {
    type: Sequelize.GEOMETRY,
    allowNull: false
  }
});
const router = require('express').Router();
const {
  getInterestGeoById,
  getAllInterestGeo,
  postInterestGeo,
  updateInterestGeo,
  deleteInterestGeo
} = require('../controllers/geo/interests');

router.get('/:objectId', getInterestGeoById);
router.get('/', getAllInterestGeo);
router.post('/', postInterestGeo);
router.put('/', updateInterestGeo);
router.delete('/:objectId', deleteInterestGeo);

module.exports = router;
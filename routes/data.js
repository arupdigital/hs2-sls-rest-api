const router = require('express').Router();
const { getById } = require('../controllers/arango/data');

router.get('/:collection/:id', getById);

module.exports = router;
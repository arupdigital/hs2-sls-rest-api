const router = require('express').Router();
const interestRouter = require('./interests');

router.use('/interest', interestRouter);

module.exports = router;
const router = require('express').Router();
const { fetchLicenceTracker, fetchWeeklyReport } = require('../controllers/arango/reports');

router.get('/licenceTracker', fetchLicenceTracker);
router.get('/weekly', fetchWeeklyReport);

module.exports = router;



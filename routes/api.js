const router = require('express').Router();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../openapi.json');
const dataRouter = require('./data');
const routes = require('../controllers/arango');
const geoRouter = require('./geo');
const reportRouter = require('./report');


Object.keys(routes).forEach(route => {
  const controllers = routes[route];
  Object.keys(controllers).forEach(contrName => {
    const { controller, path, method } = controllers[contrName];
    router[method](`/${route}${path}`, controller);
  });
});


router.use('/explore', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
router.use('/data', dataRouter);
router.use('/geo', geoRouter);
router.use('/report', reportRouter);

module.exports = router;